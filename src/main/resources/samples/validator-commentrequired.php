<?php
$comment = $transientVars["comment"];
if (!$comment) {
    $errors[""] = "Validation failed";
    $errors["comment"] = "Comment is required";
}
?>