package ua.com.able.jira.plugins.phprunner.rest;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class EntityRunRequest {

	@XmlElement
	private String inlineScript;

    @XmlElement
	private String filePath;

    @SuppressWarnings("unused")
	private EntityRunRequest() {
	}

    public EntityRunRequest(String inlineScript, String filePath) {
		this.inlineScript = inlineScript;
		this.filePath = filePath;
	}

	public String getInlineScript() {
		return inlineScript;
	}

	public void setInlineScript(String inlineScript) {
		this.inlineScript = inlineScript;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

}
