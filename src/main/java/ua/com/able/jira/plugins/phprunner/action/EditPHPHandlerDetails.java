package ua.com.able.jira.plugins.phprunner.action;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.atlassian.configurable.ObjectConfigurationException;
import com.atlassian.jira.plugins.mail.webwork.AbstractEditHandlerDetailsWebAction;
import com.atlassian.jira.service.JiraServiceContainer;
import com.atlassian.jira.service.services.file.AbstractMessageHandlingService;
import com.atlassian.jira.service.util.ServiceUtils;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.webresource.api.assembler.PageBuilderService;

import ua.com.able.jira.plugins.phprunner.mail.PHPMailHandler;
import ua.com.able.jira.plugins.phprunner.utils.PHPRunnerUtils;

public class EditPHPHandlerDetails extends AbstractEditHandlerDetailsWebAction {

	private static final long serialVersionUID = -3054068729368445193L;
	private static Logger log = Logger.getLogger(EditPHPHandlerDetails.class);
    private final PageBuilderService pageBuilderService;
	private String inlineSrc;
	private String fileSrc;
	private String catchemail;
	private String forwardEmail;
	private boolean stripquotes;

	public EditPHPHandlerDetails(PluginAccessor pluginAccessor,
			final PageBuilderService pageBuilderService) {
		super(pluginAccessor);
        this.pageBuilderService = pageBuilderService;
        this.pageBuilderService.assembler().resources()
	    	.requireWebResource("ua.com.able.jira.plugins.phprunner:phprunner-resources")
	    	.requireWebResource("ua.com.able.jira.plugins.phprunner:codemirror");
	}

	@Override
	protected void copyServiceSettings(JiraServiceContainer jiraServiceContainer) throws ObjectConfigurationException {
		// from database to our instance
		log.warn("copyServiceSettings()");
		final String params = jiraServiceContainer.getProperty(AbstractMessageHandlingService.KEY_HANDLER_PARAMS);
		final Map<String, String> parameterMap = ServiceUtils.getParameterMap(params);
		inlineSrc = PHPRunnerUtils.emptyNotNull(parameterMap.get(PHPMailHandler.KEY_INLINE_SCRIPT));
		fileSrc = PHPRunnerUtils.emptyNotNull(parameterMap.get(PHPMailHandler.KEY_FILE_PATH));
		catchemail = PHPRunnerUtils.emptyNotNull(parameterMap.get(PHPMailHandler.KEY_CATCHEMAIL));
		forwardEmail = PHPRunnerUtils.emptyNotNull(parameterMap.get(PHPMailHandler.KEY_FORWARD_EMAIL));
		stripquotes = new Boolean(parameterMap.get(PHPMailHandler.KEY_STRIP_QUOTES));
	}

	@Override
	protected Map<String, String> getHandlerParams() {
		log.warn("getHandlerParams()");
		Map<String, String> map = new HashMap<String, String>();
		if (!PHPRunnerUtils.isNullOrEmpty(inlineSrc))  map.put(PHPMailHandler.KEY_INLINE_SCRIPT, inlineSrc);
		if (!PHPRunnerUtils.isNullOrEmpty(fileSrc)) map.put(PHPMailHandler.KEY_FILE_PATH, fileSrc);
		if (!PHPRunnerUtils.isNullOrEmpty(catchemail))  map.put(PHPMailHandler.KEY_CATCHEMAIL, catchemail);
		if (!PHPRunnerUtils.isNullOrEmpty(forwardEmail)) map.put(PHPMailHandler.KEY_FORWARD_EMAIL, forwardEmail);
		if (stripquotes) map.put(PHPMailHandler.KEY_STRIP_QUOTES, new Boolean(stripquotes).toString());
		return map;
	}

	@Override
	protected void doValidation() {
		if (configuration == null) {
			return; // short-circuit in case we lost session, goes directly to doExecute which redirects user
		}
		super.doValidation();
        if (inlineSrc == null || fileSrc == null) {
            addErrorMessage("Both inline script and file script path must be set");
            return;
        }

	}

	public String getFileSrc() {
		return fileSrc;
	}

	public void setFileSrc(String fileSrc) {
		this.fileSrc = fileSrc.trim();
	}

	public String getInlineSrc() {
		if (!PHPRunnerUtils.isNullOrEmpty(inlineSrc)) {
			return inlineSrc;
		}
		return PHPRunnerUtils.readResource("/templates/phprunner/admin/phpmailhandlerdefaultconfig.php.txt");
	}

	public void setInlineSrc(String inlineSrc) {
		this.inlineSrc = inlineSrc.trim();
	}

	public String getCatchemail() {
		return catchemail;
	}

	public void setCatchemail(String catchemail) {
		this.catchemail = catchemail.trim();
	}

	public String getForwardEmail() {
		return forwardEmail;
	}

	public void setForwardEmail(String forwardEmail) {
		this.forwardEmail = forwardEmail.trim();
	}

	public boolean isStripquotes() {
		return stripquotes;
	}

	public void setStripquotes(boolean stripquotes) {
		this.stripquotes = stripquotes;
	}

}
