package ua.com.able.jira.plugins.phprunner.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import com.atlassian.webresource.api.assembler.PageBuilderService;

@WebSudoRequired
public class PHPRunnerConsole extends JiraWebActionSupport {

	private static final long serialVersionUID = 1930062132175738102L;
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(PHPRunnerConsole.class);
    private final PageBuilderService pageBuilderService;

    public PHPRunnerConsole(final PageBuilderService pageBuilderService) {
    	super();
        this.pageBuilderService = pageBuilderService;
        //log.warn(this.pageBuilderService.assembler().resources().toString());
    }

    @Override
	protected String doExecute() throws Exception {
        //log.warn("Entering doExecute");
        this.pageBuilderService.assembler().resources()
	    	.requireWebResource("ua.com.able.jira.plugins.phprunner:phprunner-resources")
	    	.requireWebResource("ua.com.able.jira.plugins.phprunner:codemirror");
        return INPUT;
    }

}
