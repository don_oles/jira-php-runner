package ua.com.able.jira.plugins.phprunner.workflow;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;

import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.SimpleBindings;

import org.apache.log4j.Logger;

import com.atlassian.jira.workflow.WorkflowFunctionUtils;
import com.caucho.quercus.env.Value;
import com.caucho.quercus.script.QuercusScriptEngine;
import com.google.common.base.Strings;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;

import ua.com.able.jira.plugins.phprunner.quercus.ScriptingManager;

public class PHPPostFunction implements FunctionProvider {

	private static Logger log = Logger.getLogger(PHPPostFunction.class);
	private final ScriptingManager scriptingManager;

	public PHPPostFunction(final ScriptingManager scriptingManager) {
		this.scriptingManager = scriptingManager;
	}

	@Override
	public void execute(@SuppressWarnings("rawtypes") Map transientVars,
			@SuppressWarnings("rawtypes") Map args, PropertySet ps)
			throws WorkflowException
	{

//    	PrintWriter stdoutWriter = new PrintWriter(System.out, true);
    	PrintWriter stderrWriter = new PrintWriter(System.err, true);
		StringWriter stdoutWriter = new StringWriter();
//		StringWriter stderrWriter = new StringWriter();

    	@SuppressWarnings("unused")
		Value value = null;

		try {
			QuercusScriptEngine quercusScriptEngine = scriptingManager.getQuercusScriptEngine();
		    Bindings bindings = new SimpleBindings();

		    bindings.put("log", log);
		    bindings.put("transientVars", transientVars);
		    bindings.put("propertySet", ps);
		    bindings.put("args", args);
		    bindings.put("issue", transientVars.get("issue"));
		    bindings.put("currentUser", WorkflowFunctionUtils.getCallerUserFromArgs(transientVars, args));
		    //bindings.put("stackTrace", Thread.currentThread().getStackTrace());

		    quercusScriptEngine.getContext().setWriter(stdoutWriter);
		    quercusScriptEngine.getContext().setErrorWriter(stderrWriter);
		    quercusScriptEngine.getContext().setBindings(bindings, ScriptContext.ENGINE_SCOPE);

		    String inlineScript = Strings.nullToEmpty((String)args.get(AbstractPHPWorkflowPluginFactory.SOURCE_INLINE));
		    String fileScript = Strings.nullToEmpty((String)args.get(AbstractPHPWorkflowPluginFactory.SOURCE_FILE));

		    value = scriptingManager.eval(inlineScript,fileScript,quercusScriptEngine.getContext());
//		    value = scriptingManager.eval(inlineScript,fileScript);

		} catch (Exception ex) {
			log.error("Unrecognized exception while executing PHP script: " + ex.getMessage(), ex);
			throw new WorkflowException(ex);
		}

		stdoutWriter.flush();
		stderrWriter.flush();

		if (log.isDebugEnabled()) {
			log.debug("stdout: "  + stdoutWriter.toString());
		}
	}

}
