package ua.com.able.jira.plugins.phprunner;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.event.events.PluginEnabledEvent;

import ua.com.able.jira.plugins.phprunner.listen.PHPRunnerSystemListener;
import ua.com.able.jira.plugins.phprunner.quercus.JiraModule;
import ua.com.able.jira.plugins.phprunner.quercus.ScriptingManager;

public class PluginInitializer implements InitializingBean, DisposableBean {

	private static Logger log = Logger.getLogger(PluginInitializer.class);

	private EventPublisher eventPublisher;
	private PHPRunnerSystemListener listener;
	private final ScriptingManager scriptingManager;
	@SuppressWarnings("unused")
	private final JiraModule jiraModule;

	public PluginInitializer(final EventPublisher eventPublisher,
			final PHPRunnerSystemListener listener,
			final ScriptingManager scriptingManager,
			final JiraModule jiraModule) {
		this.eventPublisher = eventPublisher;
		this.listener = listener;
		this.scriptingManager = scriptingManager;
		this.jiraModule = jiraModule;
		jiraModule.injectScriptingManager(scriptingManager);

		scriptingManager.create(); // MUST be created in constructor with the
									// OSGI class loader
		log.info("PluginInitializer()");
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		// log.info("Registering temporary listener");
		eventPublisher.register(this);
	}

	@EventListener
	public void onEvent(PluginEnabledEvent event) {
		Plugin plugin = event.getPlugin();

		log.setLevel(Level.INFO);
		if (!plugin.getKey().equals("ua.com.able.jira.plugins.phprunner")) {
			return;
		}

		try {
			scriptingManager.install();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		} finally {
			eventPublisher.unregister(this);
			eventPublisher.register(this.listener);
			scriptingManager.init();
			log.info("Registered listener, initialization finished");
		}

	}

	@Override
	public void destroy() throws Exception {
		log.info("Unregistering listener");
		if (listener != null)
			eventPublisher.unregister(listener);
	}

}
