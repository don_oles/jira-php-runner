package ua.com.able.jira.plugins.phprunner.rest;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class EntityRunResponse {

    @XmlElement
	private String stdout;

    @XmlElement
	private String stderr;

	@SuppressWarnings("unused")
	private EntityRunResponse() {
	}

	public EntityRunResponse(String stdout, String stderr) {
		this.stdout = stdout;
		this.stderr = stderr;
	}

	public String getStdout() {
		return stdout;
	}

	public void setStdout(String stdout) {
		this.stdout = stdout;
	}

	public String getStderr() {
		return stderr;
	}

	public void setStderr(String stderr) {
		this.stderr = stderr;
	}





}
