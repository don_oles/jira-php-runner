package ua.com.able.jira.plugins.phprunner.rest;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.SimpleBindings;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.sal.api.websudo.WebSudoSessionException;
import com.caucho.quercus.env.Value;
import com.caucho.quercus.script.QuercusScriptEngine;
import com.google.common.base.Strings;

import ua.com.able.jira.plugins.phprunner.quercus.ScriptingManager;

public class PHPRunnerRestServlet extends HttpServlet  {

	private static final long serialVersionUID = -4426615909730304519L;
	private static final Logger log = LoggerFactory.getLogger(PHPRunnerRestServlet.class);
    private final WebSudoManager webSudoManager;
    private final ScriptingManager scriptingManager;

	public PHPRunnerRestServlet (final WebSudoManager webSudoManager,final ScriptingManager scriptingManager) {
		super();
		this.scriptingManager = scriptingManager;
		this.webSudoManager = webSudoManager;
	}

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
    	log.debug("Entering doGet");

        try {
            webSudoManager.willExecuteWebSudoRequest(req);

            resp.setContentType("application/json;charset=UTF-8");
    		resp.getWriter().write(getJsonResponseBody("This is STDOUT","This is STDERR"));
        } catch(WebSudoSessionException wes) {
            webSudoManager.enforceWebSudoProtection(req, resp);
        }
    }

    @Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {

        try {
            webSudoManager.willExecuteWebSudoRequest(req);

	    	BufferedReader br = req.getReader();
	        StringBuilder sb = new StringBuilder();
	        String str;
	        while( (str = br.readLine()) != null ){
	            sb.append(str);
	        }
	        JSONObject jObj = new JSONObject(sb.toString());
	        String inlineScript = jObj.getString("inlineScript");
	        String filePath = jObj.getString("filePath");

			StringWriter stdoutWriter = new StringWriter();
			StringWriter stderrWriter = new StringWriter();

		    Value value = null;

			try {

				QuercusScriptEngine quercusScriptEngine = scriptingManager.getQuercusScriptEngine();
			    Bindings bindings = new SimpleBindings();
			    quercusScriptEngine.setBindings(bindings, ScriptContext.ENGINE_SCOPE);
			    quercusScriptEngine.getContext().setWriter(stdoutWriter);
			    quercusScriptEngine.getContext().setErrorWriter(stderrWriter);

			    if (!Strings.isNullOrEmpty(inlineScript)) {
			    	value = (Value)quercusScriptEngine.eval("<?php " + inlineScript + " ?>");
				    log.debug("inlineScript return value: " + value);

			    }
			    if (!Strings.isNullOrEmpty(filePath)) {
			    	value = (Value) quercusScriptEngine.eval(new FileReader(scriptingManager.getScriptRoot() + "/" + filePath));
			    }

			} catch (Exception ex) {
				log.error("Unrecognized exception while executing PHP script: " + ex.getMessage(), ex);
				stderrWriter.append(ex.getMessage());
			}

			stdoutWriter.flush();
			stderrWriter.flush();

	    	log.debug("Entering doGet:" + jObj);
			resp.setContentType("application/json;charset=UTF-8");
			resp.getWriter().write(getJsonResponseBody(stdoutWriter.toString(),stderrWriter.toString()));
        } catch(WebSudoSessionException wes) {
            resp.sendError(HttpServletResponse.SC_FORBIDDEN);
        }
    }

    private String getJsonResponseBody(String stdout, String stderr) {
    	Map<String,String> map = new HashMap<String,String>();
    	map.put("stdout", stdout);
    	map.put("stderr", stderr);
    	return new JSONObject(map).toString();
    }

}
