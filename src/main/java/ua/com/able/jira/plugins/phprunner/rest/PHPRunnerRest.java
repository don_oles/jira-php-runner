package ua.com.able.jira.plugins.phprunner.rest;

import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;

import java.io.StringWriter;
import java.util.List;

import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.SimpleBindings;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.CacheControl;

import org.apache.log4j.Logger;

import com.caucho.quercus.env.Value;
import com.caucho.quercus.script.QuercusScriptEngine;

import ua.com.able.jira.plugins.phprunner.quercus.ScriptingManager;
import ua.com.able.jira.plugins.phprunner.utils.PHPRunnerUtils;

@Path("/")
public class PHPRunnerRest {

	private static Logger log = Logger.getLogger(PHPRunnerRest.class);
	private final ScriptingManager scriptingManager;
    private final UserManager userManager;

	public PHPRunnerRest(final ScriptingManager scriptingManager, final UserManager userManager) {
		this.scriptingManager = scriptingManager;
        this.userManager = userManager;
	}

	@GET
	@Path("test")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response test() {
		return Response.ok(new EntityRunResponse("This is STDOUT", "This is STDERR")).build();
	}

	@GET
	@Path("scripts")
	@Produces({ MediaType.APPLICATION_JSON })
	// /jira/rest/phprunner/1.0/scripts
	public Response scripts() {

		UserKey userKey = userManager.getRemoteUserKey();
        if (userKey == null) {
            return Response.status(401).cacheControl(never()).build();
        }
        if (!userManager.isAdmin(userKey)) {
            return Response.status(403).cacheControl(never()).build();
        }

        List<String> scripts = PHPRunnerUtils.getScriptRootFiles();
		return Response.ok(new ScriptsResponseEntity(scripts.toArray(new String[0]))).build();
	}

	@POST
	@Path("run")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response run(final EntityRunRequest runRequest) {

		UserKey userKey = userManager.getRemoteUserKey();
        if (userKey == null) {
            return Response.status(401).cacheControl(never()).build();
        }
        if (!userManager.isAdmin(userKey)) {
            return Response.status(403).cacheControl(never()).build();
        }

        StringWriter stdoutWriter = new StringWriter();
		StringWriter stderrWriter = new StringWriter();

		@SuppressWarnings("unused")
		Value value = null;

		try {

			QuercusScriptEngine quercusScriptEngine = scriptingManager.getQuercusScriptEngine();
			Bindings bindings = new SimpleBindings();
			ScriptContext context = quercusScriptEngine.getContext();
			context.setWriter(stdoutWriter);
			context.setErrorWriter(stderrWriter);
			context.setBindings(bindings, ScriptContext.ENGINE_SCOPE);
			value = scriptingManager.eval(runRequest.getInlineScript(), runRequest.getFilePath(), context);

		} catch (Exception ex) {
			log.error("Exception while executing PHP script: " + ex.getMessage(), ex);
			stderrWriter.append(ex.getMessage());
			stderrWriter.append("<br>");
			for (StackTraceElement el : ex.getStackTrace()) {
				stderrWriter.append(el.toString());
				stderrWriter.append("<br>");
			}
		}

		stdoutWriter.flush();
		stderrWriter.flush();

		return Response.ok(new EntityRunResponse(stdoutWriter.toString(), stderrWriter.toString())).build();
	}

	private static CacheControl never()
    {
        CacheControl cacheNever = new javax.ws.rs.core.CacheControl();
        cacheNever.setNoStore(true);
        cacheNever.setNoCache(true);
        return cacheNever;
    }

}