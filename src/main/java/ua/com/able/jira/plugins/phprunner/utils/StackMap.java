/*
 * from http://jxl.cvs.sourceforge.net/
 *
 * fixed by Oles
 *
 * StackMap.java
 *
 * Created on March 21, 2004, 7:49 AM
 */

package ua.com.able.jira.plugins.phprunner.utils;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import javax.naming.OperationNotSupportedException;

/**
 *
 * @author  Administrator
 */

/** A stack based implementation of the Map interface.
 * Comment This Class will be moved to a util package in future a releases
 * Also, most of the optional methods in the map return null. (I know thats wrong.
 * Sigh...)
 *
 * Feel free to correct or implement these methods, but please do not allow
 * Iterators to be mutable (do not support remove()). Thanks
 */
public class StackMap<K,V> implements Map<K,V> {

    /** Creates a new instance of StackMap */
    public StackMap() {
        keys = new Stack<K>();
        values = new Stack<V>();
    }
    private Stack<K> keys;
    private Stack<V> values;

    @Override
	public void clear() {
        keys = new Stack<K>();
        values = new Stack<V>();
    }

    @Override
	public boolean containsKey(Object key) {
        return (keys.search(key) == -1) ? false : true;
    }

    @Override
	public boolean containsValue(Object value) {
        return (values.search(value) == -1) ? false : true;
    }

    @Override
	public Set<Map.Entry<K,V>> entrySet() {
        throw new RuntimeException(new OperationNotSupportedException());
    }

    @Override
	public V get(Object key) {
        synchronized(keys){
            int i = keys.search(key);
            if(i == -1) return null;
            i = (size()-i);
            return values.get(i);
        }
    }

    @Override
	public boolean isEmpty() {
        return keys.isEmpty();
    }

    @Override
	public Set<K> keySet() {
        return Collections.unmodifiableSet(new HashSet<K>(keys));
    }
    /**
     * removes the last key-value pair in this map
     * @return returns the value Object of the key-value pair that is removed
     */
    public V removeLast(){
        synchronized(keys){
            if(!isEmpty()){
                V o = null;
                try {
                    o = remove(keys.get(0));
                }catch(IndexOutOfBoundsException ioobe){
                    //This happens somethimes ???!!!
                }
                return o;
            }else{
                return null;
            }
        }
    }

    public K removeLastAndGetKey(){
        synchronized(keys){
            if(!isEmpty()){
                K k = null;
                try {
                	k = getKeyAt(0);
                    remove(keys.get(0));
                }catch(IndexOutOfBoundsException ioobe){
                    //This happens somethimes ???!!!
                }
                return k;
            }else{
                return null;
            }
        }
    }

    public K getFirstKey(){
        synchronized(keys){
            if(!isEmpty()){
                K o = null;
                try{
                    o = keys.get(size());
                }catch(IndexOutOfBoundsException ioobe){
                    //This happens somethimes ???!!!
                }
                return o;
            }else{
                return null;
            }
        }
    }
    public K getLastKey(){
        synchronized(keys){
            if(!isEmpty()){
                K o = null;
                try{
                    o = keys.get(0);
                }catch(IndexOutOfBoundsException ioobe){
                    //This happens somethimes ???!!!
                }
                return o;
            }else{
                return null;
            }
        }
    }
    public V getFirstValue(){
        synchronized(keys){
            if(!isEmpty()){
                V o = null;
                try{
                    o = values.get(size());
                }catch(IndexOutOfBoundsException ioobe){
                    //This happens somethimes ???!!!
                }
                return o;
            }else{
                return null;
            }
        }

    }
    public V getLastValue(){
        synchronized(keys){
            if(!isEmpty()){
                V o = null;
                try{
                    o = values.get(0);
                }catch(IndexOutOfBoundsException ioobe){
                    //This happens somethimes ???!!!
                }
                return o;
            }else{
                return null;
            }
        }
    }

    @Override
	public V put(K key, V value) {
        synchronized(keys){
            values.push(value); // no search for pre existing value. OK!
            keys.push(key);
        }
        return null; // don't replace so always return null;
    }
    /** moves key and the value it is bound with to the top of the StackMap */
    public void moveToTop(Object key){
        synchronized(keys){
            int i = keys.search(key);
            i = (size()-i);
            keys.push(keys.remove(i));
            values.push(values.remove(i));
        }
    }

    @Override
	public  void putAll(Map<? extends K,? extends V> t){
        throw new RuntimeException(new OperationNotSupportedException());
    }

    @Override
	public V remove(Object key) {
        synchronized(keys){
            int i = keys.search(key);
            if(i == -1) return null;
            i = (size()-i);
            keys.remove(i);
            return values.remove(i);
        }
    }

    @Override
	public int size() {
        return values.size();
    }

    @Override
	public Collection<V> values() {
        return Collections.unmodifiableCollection(values);
    }

    public K getKeyAt(int index) {
        synchronized(keys){
            return keys.elementAt(index);
        }
    }

    public V getValueAt(int index) {
        synchronized(keys){// using keys as the default lock object
            return values.elementAt(index);
        }
    }

    public void setKeyAt(K o, int index) {
        synchronized(keys){// using keys as the default lock object
            keys.setElementAt(o,index);
        }
    }

    public void setValueAt(V o, int index) {
        synchronized(keys){// using keys as the default lock object
            values.setElementAt(o,index);
        }
    }

}
