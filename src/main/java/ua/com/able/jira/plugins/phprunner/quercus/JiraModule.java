package ua.com.able.jira.plugins.phprunner.quercus;

import com.caucho.quercus.env.Env;
import com.caucho.quercus.env.StringValue;
import com.caucho.quercus.env.Value;
import com.caucho.quercus.module.AbstractQuercusModule;
import com.opensymphony.util.TextUtils;

import ua.com.able.jira.plugins.phprunner.utils.PHPRunnerUtils;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.jfree.util.Log;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.util.ComponentLocator;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.system.ExtendedSystemInfoUtils;
import com.atlassian.jira.util.system.ExtendedSystemInfoUtilsImpl;

public class JiraModule extends AbstractQuercusModule {

	private static Logger log = Logger.getLogger(JiraModule.class);
	private static ScriptingManager scriptingManager;

	public JiraModule() {
		log.warn("JiraModule()");
	}

	// this one cannot have name starting with 'get', as OSGI will try to inject
	// using it.
	public void injectScriptingManager(ScriptingManager sc) {
		scriptingManager = sc;
	}

	@Override
	public String[] getLoadedExtensions() {
		return new String[] { "jira" };
	}

//	public void hello_jira(Env env, String str) {
//		env.println("hello from jira: " + str);		// 'echos' the string
//	}

	public void jira_info(Env env) {
		I18nHelper i18Helper = ComponentAccessor.getJiraAuthenticationContext().getI18nHelper();
		ExtendedSystemInfoUtils extendedSystemInfoUtils = new ExtendedSystemInfoUtilsImpl(i18Helper);
		Map<String, String> bs = extendedSystemInfoUtils.getBuildStats();
		env.println("<b>JIRA Info</b><br>");
		for (String key : bs.keySet()) {
			env.println(key + ": " + bs.get(key) + "<br>");
		}
	}

	public void phprunner_info(Env env) {

		if (scriptingManager == null) {
			env.error("scriptingManager not injected!");
			return;
		}

		env.println("<b>PHP Runner Info</b><br>");
		env.println("Cache size: " + scriptingManager.getMaxScriptCacheSize() + "<br>");
		env.println("Cached scripts: " + scriptingManager.getScriptCacheSize() + "<br>");
		env.println("Total compile time (ms): " + scriptingManager.getTotalCompileTime() + "<br>");
		env.println("Total eval time (ms): " + scriptingManager.getTotalEvalTime() + "<br>");
		env.println("Total evals: " + scriptingManager.getEvals() + "<br>");
		env.println("Script root: " + TextUtils.htmlEncode(PHPRunnerUtils.getScriptRoot()) + "<br>");
	}

	public void phprunner_clearcache(Env env) {
		Log.warn("scriptingManager:" + scriptingManager.toString());
		scriptingManager.clearScriptCache();
	}

	public Value java_stack_trace(Env env) {
		return env.wrapJava(Thread.currentThread().getStackTrace());
	}

	public Value java_class_forname(Env env, StringValue str) {
		String clazzName = str.toJavaString();
		log.warn("class_forname: " + clazzName);
		try {
			return env.wrapJava(Class.forName(clazzName,false,ComponentLocator.class.getClassLoader()));
		} catch (ClassNotFoundException e) {
			return env.wrapJava(null);
		}
	}

	public Value jira_component(Env env, StringValue str) {
		String clazzName = str.toJavaString();
		log.warn("class_forname: " + clazzName);
		try {
			return env.wrapJava(ComponentAccessor.getComponent(Class.forName(clazzName,false,ComponentLocator.class.getClassLoader())));
		} catch (ClassNotFoundException e) {
			return env.wrapJava(null);
		}
	}

	public Value jira_java_string_map(Env env) {
		return env.wrapJava(new HashMap<String,Object>());
	}
}
