JIRA PHP Runner Plugin
======================

# Description
The PHP runner plugin for JIRA allows creating PHP scripted post-functions, validators, conditions, listeners, and also has a console. Plus an experimental mail handler. It uses the [Quercus](http://quercus.caucho.com/) scripting engine to execute PHP. Plus an experimental emulation of a web-server serving php files along with some static files having extentions html, htm, txt, js, css, jpeg, png, gif.

# Taste of it
A sample script might look like:
```php
import com.atlassian.jira.component.ComponentAccessor;
$customFieldManager = ComponentAccessor::getCustomFieldManager(); // static method
$fieldObj = $customFieldManager->getCustomFieldObject("customfield_10000");
$value = $issue->getCustomFieldValue($fieldObj);
```

# Why PHP Runner?
The [ScriptRunner for JIRA](https://marketplace.atlassian.com/plugins/com.onresolve.jira.groovy.groovyrunner/server/overview) is much more powerful, includes the Behaviours plugin, but it is commercial now, and the last free version 3.0.16 won't run JIRA 7.0. There's also [JIRA Scripting Suite](https://marketplace.atlassian.com/plugins/com.quisapps.jira.jss/server/overview) plugin which uses Jython, but it's Python, you know. And PHP is a doll! PHP Runner was started out of curiosity if it is possible to adapt Quercus to a JIRA plugin, so it is a proof of concept.

It runs OK on test instances of JIRA 6.3, 6.4, 7.0, and the PHP Mail Handler is proven in production environment.

Quercus PHP is not pure vanilla PHP, of course, keep it in mind. At the moment it's bundled with Quercus version 4.0

Performance is surprisingly OK for the purpose.

# Major pitfalls
The main problem with using Quercus as PHP (no other alternative) which you may encounter is when you try using JIRA JAVA API which references classes in other plugins, for instance `CustomFieldType` classes. Since every JIRA plugin is essentially an OSGI bundle, every one of them has its own classloader and its resources are 'protected' from being accessed by other plugins. As a result, when Quercus tries to marshal a class which belongs to another plugin (wrap it) it throws `java.lang.ClassNotFoundException` exception.

Another problem is that you cannot get class object by name, so `ComponentAccessor.getComponent()` won't work as you cannot pass a class as a parameter. A workaround for this is to use bundled with the plugin `jira_module`, which has `jira_component($clazzName)` function basically returning `ComponentAccessor.getComponent(Class.forName(clazzName,false,ComponentLocator.class.getClassLoader()))`.

# Compiling
Just `git fetch` and `atlas-package` (having Atlassian SDK, of course).

But before compiling you need to get the `quercus.jar` and feed it to the `maven`.
```
git clone https://github.com/mdaniel/svn-caucho-com-resin.git
cd svn-caucho-com-resin
ant quercus.war
mvn install:install-file -Dfile=dist/quercus-war-4.0.s<BLAHBLAH>/WEB-INF/lib/quercus.jar -DgroupId=com.caucho -DartifactId=quercus -Dversion=4.0-SNAPSHOT -Dpackaging=jar
```

# Using
When fist installed and enabled the plugin will create `jira-home/quercus` directory with `php.ini`, and this directory will server as `script root`. In the Add-ons menu you will see two items: `PHP Console` and `PHP Listeners`.

When specifying scripts, unlike in other plugins, you may enter both script in a textarea and a path to PHP script file relative to `script root`. When executing, the plugin will combine both scripts into a single page, compile it, save it in a cache of compiled scripts keyed by md5 hash of the resulting script, and evaluate. That is to say, if you `require` something from a script, its parsed form won't be cached. The resulting script will look like:
```php
<? set_include_path(your_script_root);
... script from the textarea
?> ... script from the file contents
```
Obviously, the script from the textarea must not contain `<?php ?>` tags, and the file must have them.

With such approach you can create reusable scripts expecting configuration data to come from the textarea script.

# Post-functions
The bindings for post-functions: `$issue`, `$currentUser`, `$transientVars`, `$args`, `$propertySet`, `$log`.

So you can write like
```php
$issue->key; $issue->isSubTask();
$currentUser->displayName;
$transientVars->pkey; $transientVars->actionId; $transientVars["project"]->lead; $transientVars["comment"];
$log->warn("Hello atlassian-jira.log!");
```

# Conditions
The bindings are the same as for post-functions. The only thing: the plugin expects a boolean value to be returned indicating if condition is met:
```php
// ....
return true;
```

# Validators
The bindings for validators will also include `$originalIssue` and `$errors` which is a Map in java and hash array in PHP, and adding strings to `$errors` will indicate that validation fails.
```php
$errors[""] = "Validator found problems:";
$errors["description"] = "Description must not be empty!";
$errors["customfield_10000"] = "Field must not be empty!";
```
The first one is the 'common' error message, and the others are linked to concrete fields.

# Listeners
The bindings for listeners will be `$event` and `$log`, and if this will be instance of `IsuseEvent`, also `$project` and `$issue`.
```php
$issue->key;
$project->getKey();
$event->someEventDependentMethod();
```

# PHP Console
The console does not have any bindings. It uses REST to pass scripts to the plugin for execution and get back the stdout and stderr outputs. So you may use REST to remotely execute scripts. Security is your concern.

# JiraModule
The plugin also includes a PHP extension for JIRA, which also was created as proof of concept. It might prove to be useful for something, so if you have ideas about functions in PHP that will interact in JIRA and fit to be part of module - you're welcome.
```php
jira_info();
phprunner_info();
phprunner_clearcache();
var_dump(java_stack_trace());
// no idea how to get class object from inside PHP for interface, so workarounds
var_dump(java_class_forname("com.atlassian.jira.issue.index.IssueIndexManager"));
var_dump(jira_component("com.atlassian.jira.issue.index.IssueIndexManager"));
$hashMap = jira_java_string_map(); // new HashMap<String,Object>()
```

# CompiledScript Cache
Purpose is to prevent frequently used script to be constantly recompiled, however not sure it helps much actually, since `require`d scripts won't be compiled and cached. The cash will be emptied on `ClearCacheEvent`, or you may execute `phprunner_clearcache();`. Also the web-server does not use `QuercusEngine` (JSR223) which provides `CompiledScript` objects, and calls `parse()` all the time.

You can control caching by setting `php.ini` values `phprunner_disable_script_caching=false`, `phprunner_disable_script_caching=true`, `phprunner_cache_size=100` and then disabling and re-enabling the plugin. The `phprunner_cache_size` accepts anything from 1 to 10000. Don't comment it, keep it.

In fact there's little benefit of having compiled scripts cache, as the PHP parsing is very fast, and I bet your code will spend more time in JIRA API calls rather then in the PHP parsing. Only one PHP script can be compiled at a time if cache is enabled.

# PHP Mail Handler
See separate page [PHP Mail Handler](phpmailhandler) (todo)

# Examples
A Condition to hide a transition from user in UI, but allow it to be transitioned by other scripts via API.
```php
$ingui = false;
foreach(java_stack_trace() as $element )
{
    if ($element->className == "com.atlassian.jira.rest.v2.issue.OpsbarBeanBuilder")
        $ingui = true;
}
return !$ingui;
```

A validator to require a comment.
```php
$comment = $transientVars["comment"];
if (!$comment) {
    $errors[""] = "Comment is required";
    $errors["comment"] = "Comment is required";
}
```

A post-function to resolve a parent issue.
```php
// this should go to the inline script as parameters for the transition.php.
$actionId = 5; // resolve issue
$targetIssue = $issue->parentObject;
$inputParameters = [];

// this should be reusable transition.php
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.util.ImportUtils;
import com.atlassian.jira.issue.IssueInputParametersImpl;

if (!$inputParameters)
    $inputParameters = [];
$issueInputParameters = new IssueInputParametersImpl($inputParameters);

if (!$targetUser)
    $targetUser = $currentUser;

$log->debug("transitionning ${targetIssue} as ${targetUser}, action: ${actionId}, inputParameters:${inputParameters}");

$indexManager = jira_component("com.atlassian.jira.issue.index.IssueIndexManager");
$issueService = ComponentAccessor::getIssueService();

$validationResult = $issueService->validateTransition($targetUser, $targetIssue->id, $actionId, $issueInputParameters);
$isValid = $validationResult->isValid();
$errorCollection = $validationResult->errorCollection;

if (!$isValid) {
    $log->error("Transition not validated, errorCollection:${errorCollection}");
    return;
}

$errorCollection = $issueService->transition($targetUser, $validationResult)->errorCollection;

if ($errorCollection->hasAnyErrors()) {
    $log->error("Transition failed, errorCollection:${errorCollection}");
    return;
}

// reload issue
$targetIssue = ComponentAccessor::getIssueManager()->getIssueObject($targetIssue->id);
$wasIndexing = ImportUtils::isIndexIssues();
$indexManager->reIndex($targetIssue);
ImportUtils::setIndexIssues($wasIndexing);
```

A post-function to [Send Custom Email](sendmail).

Console script to rename a workflow (even active):
```php
import com.atlassian.jira.component.ComponentAccessor;
$wm = ComponentAccessor::getWorkflowManager();
$wf = $wm->getWorkflow("FOO Workflow 1.00");
if ($wf !== null) {
    $wm->updateWorkflowNameAndDescription("admin",$wf,"FOO Workflow 1.01",$wf->getDescription());
    echo "updated";
} else {
    echo "workflow not found";
}
```