package ua.com.able.jira.plugins.phprunner.mail;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.apache.log4j.Logger;

import com.atlassian.core.util.ClassLoaderUtils;
//import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.JiraApplicationContext;
import com.atlassian.jira.JiraException;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.event.issue.IssueEventSource;
import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.SummarySystemField;
import com.atlassian.jira.issue.history.ChangeItemBean;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.priority.Priority;
import com.atlassian.jira.issue.security.IssueSecurityLevelManager;
import com.atlassian.jira.service.util.ServiceUtils;
import com.atlassian.jira.service.util.handler.MessageHandlerContext;
import com.atlassian.jira.service.util.handler.MessageHandlerExecutionMonitor;
import com.atlassian.jira.service.util.handler.MessageUserProcessor;
import com.atlassian.jira.user.ApplicationUsers;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.mail.MailUtils;

import ua.com.able.jira.plugins.phprunner.utils.PHPRunnerUtils;

import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Flags;
import com.atlassian.jira.mail.Email;
import com.atlassian.jira.plugins.mail.internal.MailLoopDetectionService;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.permission.ProjectPermissions;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import com.google.common.collect.Iterables;
import com.google.common.base.Predicate;
import com.google.common.collect.Lists;
import com.opensymphony.util.TextUtils;


import javax.annotation.Nullable;
import java.util.StringTokenizer;
import com.atlassian.jira.user.UserUtils;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.util.dbc.Assertions;
import com.atlassian.jira.exception.ParseException;
import org.apache.commons.io.IOUtils;
import com.atlassian.jira.web.FieldVisibilityManager;
import com.atlassian.jira.web.util.AttachmentException;
import com.atlassian.jira.web.util.FileNameCharacterCheckerUtil;
import com.atlassian.jira.issue.util.IssueUpdateBean;
import com.atlassian.jira.issue.util.IssueUpdater;
import com.atlassian.jira.issue.watchers.WatcherManager;

import java.util.Locale;


public class MailHandler {

	private final MessageUserProcessor messageUserProcessor;
	private final JiraApplicationContext jiraApplicationContext;
	private final ApplicationProperties applicationProperties;
	private final WatcherManager watcherManager;
    private final IssueUpdater issueUpdater;
    private final IssueSecurityLevelManager issueSecurityLevelManager;
    private final CustomFieldManager customFieldManager;
    private final FieldVisibilityManager fieldVisibilityManager;
    private final ConstantsManager constantsManager;
    private final IssueFactory issueFactory;
    private final ProjectManager projectManager;
    private final I18nHelper i18nHelper;
    private final MailLoopDetectionService mailLoopDetectionService;
    private final PermissionManager permissionManager;
    private static final FileNameCharacterCheckerUtil fileNameCharacterCheckerUtil = new FileNameCharacterCheckerUtil();

    private static final char INVALID_CHAR_REPLACEMENT = '_';
    private final static String DEFAULT_BINARY_FILE_NAME = "binary.bin";
    private static final String ATTACHED_MESSAGE_FILENAME = "attachedmessage";
    protected static final String CONTENT_TYPE_TEXT = "text/plain";
    protected static final String HEADER_MESSAGE_ID = "message-id";
    protected static final String HEADER_IN_REPLY_TO = "in-reply-to";
    private static final String OUTLOOK_QUOTED_FILE = "outlook-email.translations";

    private Map<String, String> params;

	private Collection<AttachmentWrapper> preparedAttachments;
	protected static Logger log = Logger.getLogger(MailHandler.class);
    protected boolean deleteEmail;
	private Collection<String> messages;

	private MessageHandlerContext currentContext;
	private Message currentMessage;
	private MessageHandlerExecutionMonitor currentMonitor;

	public MailHandler(final MessageUserProcessor messageUserProcessor,
			final PermissionManager permissionManager, ApplicationProperties applicationProperties,
			final IssueUpdater issueUpdater, final WatcherManager watcherManager,
			final IssueSecurityLevelManager issueSecurityLevelManager, final CustomFieldManager customFieldManager,
			final ConstantsManager constantsManager, final IssueFactory issueFactory,
			final ProjectManager projectManager, final I18nHelper i18nHelper,
			final JiraApplicationContext jiraApplicationContext) {
		this.messageUserProcessor = messageUserProcessor;
        this.jiraApplicationContext = jiraApplicationContext;
        this.applicationProperties = applicationProperties;
        this.permissionManager = permissionManager;
        this.issueUpdater = issueUpdater;
        this.watcherManager = watcherManager;
        this.issueSecurityLevelManager = issueSecurityLevelManager;
        this.customFieldManager = customFieldManager;
        this.fieldVisibilityManager = ComponentAccessor.getComponent(FieldVisibilityManager.class);
        this.constantsManager = constantsManager;
        this.issueFactory = issueFactory;
        this.projectManager = projectManager;
        this.i18nHelper = i18nHelper;
        this.mailLoopDetectionService = ComponentAccessor.getOSGiComponentInstanceOfType(MailLoopDetectionService.class);
		clearPreparedAttachments();
	}

	public void setCurrentContext(Message currentMessage, MessageHandlerContext currentContext) {
		this.currentContext = currentContext;
		this.currentMessage = currentMessage;
		this.currentMonitor = currentContext.getMonitor();
	}

	public boolean canHandleMessage(final Message message)
            throws MessagingException
    {
        log.debug("canHandleMessage params: " + params);
        deleteEmail = false;
        if(message.isSet(Flags.Flag.DELETED)) {
        	String errMessage = "This message was already deleted. Likely cause: mail handler failed to complete last time.";
            currentMonitor.messageRejected(message, errMessage);
            deleteEmail = false; // If the message is already deleted then don't delete it again.
            log.debug(errMessage);
            return false;
        }

        if (!fingerPrintCheck(message, currentMonitor)) {
        	String errMessage = "Rejecting message due to failed fingerprint check.";
        	currentMonitor.messageRejected(message, errMessage);
        	log.debug(errMessage);
            return false;
        }

        if (checkBulk(message, currentMonitor)) {
        	String errMessage = "Rejecting message due to failed bulk check.";
        	currentMonitor.messageRejected(message, errMessage);
        	log.debug(errMessage);
            return false;
        }

        String catchEmail = params.get(PHPMailHandler.KEY_CATCHEMAIL);
        if (catchEmail != null) {
            final boolean forCatchAll;
            try {
                forCatchAll = MailUtils.hasRecipient(catchEmail, message);
            } catch (final MessagingException exception) {
                deleteEmail = false;
                log.debug("Could not parse message recipients. Assuming message is bad.", exception);
                final String text = i18nHelper.getText("admin.errors.bad.destination.address");
                currentMonitor.error(text, exception);
                currentMonitor.messageRejected(message, text);
                return false;
            }

            if (!forCatchAll) {
                deleteEmail = false; // the email if its not intended for this "catchemail"
                currentMonitor.messageRejected(message, "The messages recipient(s) does not match catch mail list.");
                logCantHandleRecipients(message, currentMonitor);
                return false;
            } else {
                deleteEmail = true; // its a hit on recipient address, we want to delete this email when we are done
            }
        }

        if (getMailLoopDetectionService().isPotentialMessageLoop(message)) {
        	currentMonitor.markMessageForDeletion(i18nHelper.getText("jmp.admin.potential.mail.loop"));
        	log.debug("Potential mail loop");
            return !handleBulk(currentMonitor);
        }

        return true;
    }


    public boolean fingerPrintCheck(final Message message, MessageHandlerExecutionMonitor messageHandlerExecutionMonitor) {
        boolean fingerPrintClean = true; // until proven guilty
        final List<String> fingerPrintHeaders = getFingerPrintHeader(message);
        final String instanceFingerPrint = jiraApplicationContext.getFingerPrint();
        if (!fingerPrintHeaders.isEmpty()) {
            if (log.isDebugEnabled()) {
                log.debug("JIRA fingerprints found on on incoming email message: ");
                for (final Object fingerPrintHeader : fingerPrintHeaders) {
                    log.debug("fingerprint: " + fingerPrintHeader);
                }
            }
            String fingerPrintPolicy = params.get(PHPMailHandler.KEY_FINGER_PRINT_POLICY);
            if (fingerPrintHeaders.contains(instanceFingerPrint)) {
                log.warn("Received message carrying this JIRA instance fingerprint (" + instanceFingerPrint + ")");
                if (PHPMailHandler.VALUE_FINGER_PRINT_ACCEPT.equalsIgnoreCase(fingerPrintPolicy)) {
                    log.debug("Handler is configured to accept such messages. Beware of mail loops: JRA-12467");
                } else if (PHPMailHandler.VALUE_FINGER_PRINT_FORWARD.equalsIgnoreCase(fingerPrintPolicy)) {
                    log.debug("Forwarding fingerprinted email.");
                    messageHandlerExecutionMonitor.markMessageForDeletion(i18nHelper.getText("admin.forward.mail.loop"));
                    fingerPrintClean = false;
                } else if (PHPMailHandler.VALUE_FINGER_PRINT_IGNORE.equalsIgnoreCase(fingerPrintPolicy)) {
                    log.debug("Handler is configured to ignore this message.");
                    fingerPrintClean = false;
                }
            } else {
                log.info("Received message with another JIRA instance's fingerprint");
            }
        }
        return fingerPrintClean;
    }

    List<String> getFingerPrintHeader(final Message message) {
        List<String> headers = Collections.emptyList();
        try {
            final String[] headerArray = message.getHeader(Email.HEADER_JIRA_FINGER_PRINT);
            if (headerArray != null) {
                headers = Arrays.asList(headerArray);
            }
        } catch (final MessagingException e) {
            log.error("Failed to get mail header " + Email.HEADER_JIRA_FINGER_PRINT);
        }
        return headers;
    }

    private void logCantHandleRecipients(final Message message, MessageHandlerExecutionMonitor monitor) {
        final Address[] addresses;
        try {
            addresses = message.getAllRecipients();
        } catch (final MessagingException e) {
        	String errMessage = "Cannot handle message. Unable to parse recipient addresses.";
            monitor.info(errMessage, e);
            log.info(errMessage);
            return;
        }

        if ((addresses == null) || (addresses.length == 0)) {
        	String errMessage = "Cannot handle message.  No recipient addresses found.";
            monitor.info(errMessage);
            log.info(errMessage);
        } else {
            final StringBuilder recipients = new StringBuilder();
            for (int i = 0; i < addresses.length; i++) {
                final InternetAddress email = (InternetAddress) addresses[i];
                if (email != null) {
                    recipients.append(email.getAddress());
                    if ((i + 1) < addresses.length) {
                        recipients.append(", ");
                    }
                }
            }
            String catchEmail = params.get(PHPMailHandler.KEY_CATCHEMAIL);
            String errMessage = "Cannot handle message as the recipient(s) (" + recipients.toString() + ") do not match the catch email " + catchEmail;
            monitor.info(errMessage);
            log.info(errMessage);
        }
    }

    private boolean handleBulk(MessageHandlerExecutionMonitor messageHandlerExecutionMonitor) {
        //default action is to process the email for backwards compatibility
    	String bulk = params.get(PHPMailHandler.KEY_BULK);
        if (bulk != null) {
            if (PHPMailHandler.VALUE_BULK_IGNORE.equalsIgnoreCase(bulk)) {
                log.debug("Ignoring email with bulk delivery type");
                deleteEmail = false;
                // handled OK
                return true;
            } else if (PHPMailHandler.VALUE_BULK_FORWARD.equalsIgnoreCase(bulk)) {
                log.debug("Forwarding email with bulk delivery type");
                messageHandlerExecutionMonitor.error(i18nHelper.getText("admin.forward.bulk.mail"));
                deleteEmail = false;
                // handled OK
                return true;
            } else if (PHPMailHandler.VALUE_BULK_DELETE.equalsIgnoreCase(bulk)) {
                log.debug("Deleting email with bulk delivery type");
                deleteEmail = true;
                // handled OK
                return true;
            }
        }
        // bulk either not set or has unknown value
        return false;
    }

    protected boolean checkBulk(final Message message, MessageHandlerExecutionMonitor messageHandlerExecutionMonitor) {
        try {
            if ("bulk".equalsIgnoreCase(getPrecedenceHeader(message)) || isDeliveryStatus(message) || isAutoSubmitted(message)) {
                return handleBulk(messageHandlerExecutionMonitor);
            }
            return false;
        } catch (final MessagingException mex) {
            if (log.isDebugEnabled()) {
                log.debug("Error occurred while looking for bulk headers - assuming not bulk email: " + mex.getMessage(), mex);
            }
            return false;
        }
    }

    protected boolean isDeliveryStatus(final Message message) throws MessagingException {
        final String contentType = message.getContentType();
        if ("multipart/report".equalsIgnoreCase(MailUtils.getContentType(contentType))) {
            return contentType.toLowerCase().contains("report-type=delivery-status");
        } else {
            final String[] returnPaths = message.getHeader("Return-Path");
            if (!ArrayUtils.isEmpty(returnPaths)) {
                return Iterables.any(Lists.newArrayList(returnPaths), new Predicate<String>() {
                    @Override
                    public boolean apply(@Nullable String input) {
                        // NDR (mail bounces) are required to have null sender address, see http://en.wikipedia.org/wiki/Bounce_message#Causes_of_a_bounce_message
                        return "<>".equalsIgnoreCase(input);
                    }
                });
            }
            return false;
        }
    }

    protected String getPrecedenceHeader(final Message message) throws MessagingException {
        final String[] precedenceHeaders = message.getHeader("Precedence");
        String precedenceHeader;
        if ((precedenceHeaders != null) && (precedenceHeaders.length > 0)) {
            precedenceHeader = precedenceHeaders[0];
            if (!StringUtils.isBlank(precedenceHeader)) {
                return precedenceHeader;
            }
        }
        return null;
    }

    protected boolean isAutoSubmitted(final Message message) throws MessagingException {
        final String[] autoSub = message.getHeader("Auto-Submitted");
        if (autoSub != null) {
            for (final String auto : autoSub) {
                if (!"no".equalsIgnoreCase(auto)) {
                    return true;
                }
            }
        }
        return false;
    }

    public Issue getAssociatedIssue(final Message message) throws MessagingException {
        // Test if the message has In-Reply-To header to a message that is associated with an issue
    	Issue issue = ComponentAccessor.getMailThreadManager().getAssociatedIssueObject(message);
    	if (issue == null) {
    		log.debug("No associated issue found for message with subject '" + message.getSubject() + "'; message ignored.");
    	}
    	return issue;
    }

    public Issue findIssueObjectInString (String subject) {
		Issue issue = ServiceUtils.findIssueObjectInString(subject);
		if (issue == null) {
		    log.debug("Issue Key not found in subject '"+subject+"'. Inspecting the in-reply-to message ID.");
		}
		return issue;
	}

    protected boolean shouldAttachPlainTextParts(final Part part)
    		throws MessagingException, IOException
    {
        return !MailUtils.isContentEmpty(part);
    }

    protected boolean shouldAttachHtmlParts(final Part part)
    		throws MessagingException, IOException
    {
        return false;
    }

    public String getEmailBody(Message message) throws MessagingException
    {
    	if ("true".equals(params.get(PHPMailHandler.KEY_STRIP_QUOTES))) {
    		return stripQuotedLines(MailUtils.getBody(message));
    	}
        return MailUtils.getBody(message);
    }

    public String stripQuotedLines(String body)
    {
        if (body == null) {
            return null;
        }

        final StringTokenizer st = new StringTokenizer(body, "\n", true);
        final StringBuilder result = new StringBuilder();

        boolean strippedAttribution = false; // set to true once the attribution has been encountered
        boolean outlookQuotedLine = false; // set to true if the Microsoft Outlook reply message ("----- Original Message -----") is encountered.

        String line1;
        String line2 = null;
        String line3 = null;
        // Three-line lookahead; on each iteration, line1 may be added unless line2+line3 indicate it is an attribution
        do {
            line1 = line2;
            line2 = line3;
            line3 = (st.hasMoreTokens() ? st.nextToken() : null); // read next line
            if (!"\n".equals(line3)) {
                // Ignore the newline ending line3, if line3 isn't a newline on its own
                if (st.hasMoreTokens()) {
                    st.nextToken();
                }
            }
            if (!strippedAttribution) {
                if (!outlookQuotedLine && line1 != null) {
					for (String separator : getOutlookQuoteSeparators()) {
						int quotePosition = line1.indexOf(separator);
						if (quotePosition != -1) {
							outlookQuotedLine = true;
							// is there something before the quote separator, add it to the result
							if (quotePosition != 0) {
								result.append(StringUtils.stripEnd(line1.substring(0, quotePosition - 1), "-_"));
							}
							break; // no need to iterate more, the rest is the quoted email
						}
					}
                }

                // Found our first quoted line; the attribution line may be line1 or line2
                if (isQuotedLine(line3)) {
                    if (looksLikeAttribution(line1)) {
                        line1 = "> ";
                    } else if (looksLikeAttribution(line2)) {
                        line2 = "> ";
                    }
                    strippedAttribution = true;
                }
            }
            if (line1 != null && !isQuotedLine(line1) && !outlookQuotedLine) {
                result.append(line1);
                if (!"\n".equals(line1)) {
                    result.append("\n");
                }
            }
        }
        while (line1 != null || line2 != null || line3 != null);
        return result.toString();
    }

    private boolean looksLikeAttribution(String line) {
        return line != null && (line.endsWith(":") || line.endsWith(":\r"));
    }

    private boolean isQuotedLine(String line) {
        return line != null && (line.startsWith(">") || line.startsWith("|"));
    }

    private Collection<String> getOutlookQuoteSeparators() {
        if (messages == null) {
            messages = new LinkedList<String>();
            BufferedReader reader = null;
            try {
                // The file is assumed to be UTF-8 encoded.
                reader = new BufferedReader(new InputStreamReader(ClassLoaderUtils.getResourceAsStream(OUTLOOK_QUOTED_FILE, this.getClass()), "UTF-8"));
                String message;
                while ((message = reader.readLine()) != null) {
                    messages.add(message);
                }
            } catch (IOException e) {
                // no more properties
                log.error("Error occurred while reading file '" + OUTLOOK_QUOTED_FILE + "'.");
            } finally {
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    log.error("Could not close the file '" + OUTLOOK_QUOTED_FILE + "'.");
                }
            }
        }
        return messages;
    }

    public ApplicationUser getReporter(final Message message) throws MessagingException {
    	ApplicationUser reporter = getMessageUserProcessor().getAuthorFromSender(message);
        if (reporter == null) {
        	String reporteruserName = params.get(PHPMailHandler.KEY_REPORTER);
            if (reporteruserName != null) {
                // Sender not registered with JIRA, use default reporter
                log.info("Sender(s) " + MailUtils.getSenders(message) + " not registered in JIRA. Using configured default reporter '" + reporteruserName + "'.");
                reporter = UserUtils.getUser(reporteruserName);
            }
        }
        if (reporter == null) {
        	String errorText = "No default reporter";
        	log.warn(errorText);
        	currentMonitor.warning(errorText);
        	currentMonitor.messageRejected(message, errorText);

        }
        return reporter;
    }

    protected MessageUserProcessor getMessageUserProcessor() {
        return messageUserProcessor;
    }

    public boolean canAddComments(ApplicationUser reporter, Issue issue) {
    	return permissionManager.hasPermission(ProjectPermissions.ADD_COMMENTS, issue, reporter);
    }

    public boolean isAssignable(ApplicationUser user, Project project) {
    	return permissionManager.hasPermission(ProjectPermissions.ASSIGNABLE_USER, project, user);
    }

    public boolean canCreateAttachments(ApplicationUser reporter, Issue issue) {
    	return permissionManager.hasPermission(ProjectPermissions.CREATE_ATTACHMENTS, issue, reporter);
    }

    public boolean attachmentsAllowed() {
    	return applicationProperties.getOption(APKeys.JIRA_OPTION_ALLOWATTACHMENTS);
    }

    public boolean canCreateIssues(ApplicationUser reporter, Project project) {
    	boolean can = permissionManager.hasPermission(ProjectPermissions.CREATE_ISSUES, project, reporter);
        if (!can) {
        	String errorText = "Reporter " + reporter.getName() + " does not have permissinos to create issues in " + project.getKey();
        	log.warn(errorText);
        	currentMonitor.warning(errorText);
        	currentMonitor.messageRejected(currentMessage, errorText);
        }
    	return can;
    }

    void clearPreparedAttachments() {
    	if (preparedAttachments != null) {
        	for(AttachmentWrapper aw : preparedAttachments) {
        		aw.deleteFile(); // clear what might remain
        	}
    	}
    	preparedAttachments = new ArrayList<AttachmentWrapper>();
    }

    public Collection<AttachmentWrapper> prepareAttachmentsForMessage(final Message message, final Issue issue)
        throws IOException, MessagingException
    {
    	clearPreparedAttachments();
        final Object messageContent = message.getContent();
        if (messageContent instanceof Multipart) {
            handleMultipart((Multipart) messageContent, message, currentContext);
        } else if (Part.ATTACHMENT.equalsIgnoreCase(message.getDisposition())) {
            //JRA-12123: Message is not a multipart, but it has a disposition of attachment.  This means that
            //we got a message with an empty body and an attachment.  We'll ignore inline.
            log.debug("Trying to add attachment to issue from attachment only message.");
            handlePart(message, null, currentContext);
        }
        addMessageAsAttachment(message);
    	return preparedAttachments;
    }

    private void addMessageAsAttachment(final Message message) throws IOException, MessagingException {
        File tempFile = null;
        try {
            tempFile = createTempFile();
        } catch (IOException ex) {
            throw new IOException("Unable to create a temporary file to copy attachment into.", ex);
        }

        final FileOutputStream out = new FileOutputStream(tempFile);
        try {
            message.writeTo(out);
        } catch (IOException ex) {
            tempFile.delete();
            throw new IOException("Error while reading full message, resulting file was incomplete and has been removed.", ex);
        } catch  (MessagingException ex) {
            tempFile.delete();
            throw ex;
        } finally {
            IOUtils.closeQuietly(out);
        }

        AttachmentWrapper aw = new AttachmentWrapper(tempFile, "original-email.eml", "message/rfc822");
        aw.skip("Original message");
        preparedAttachments.add(aw);
    }

    public AttachmentWrapper addAttachment(String fileName, String filePath, String contentType) throws IOException, MessagingException {
        File file = new File(filePath);
        AttachmentWrapper aw = new AttachmentWrapper(file, fileName, contentType);
        preparedAttachments.add(aw);
        return aw;
    }

    final protected boolean shouldAttach(final Part part, final Message containingMessage)
            throws MessagingException, IOException
    {
        Assertions.notNull("part", part);
        boolean should;
        if (log.isDebugEnabled()) {
            log.debug("Checking if attachment should be added to issue, Content-Type:" + part.getContentType()+" Content-Disposition:"+part.getDisposition());
        }
        if (isJiraGeneratedAttachment(part)) {
            log.debug("Attachment detected as JIRA generated attachment - skipping.");
            should = false;
        } else if (MailUtils.isPartMessageType(part) && (null != containingMessage)) {
            log.debug("Attachment detected as a rfc/822 message.");
            should = shouldAttachMessagePart(part, containingMessage);
        } else if (isPartAttachment(part)) {
            log.debug("Attachment detected as an \'Attachment\'.");
            should = shouldAttachAttachmentsParts(part);
        } else if (MailUtils.isPartInline(part)) {
            log.debug("Attachment detected as an inline element.");
            should = shouldAttachInlineParts(part);
        } else if (MailUtils.isPartPlainText(part)) {
            log.debug("Attachment detected as plain text.");
            should = shouldAttachPlainTextParts(part);
        } else if (MailUtils.isPartHtml(part)) {
            log.debug("Attachment detected as HTML.");
            should = shouldAttachHtmlParts(part);
        } else if (MailUtils.isPartRelated(containingMessage)) {
            log.debug("Attachment detected as related content.");
            should = shouldAttachRelatedPart(part);
        } else {
            should = false;
        }
        if (log.isDebugEnabled()) {
            if (should) {
                log.debug("Attachment was added to issue");
            } else {
                log.debug("Attachment was ignored.");
            }
        }
        return should;
    }

    private boolean isJiraGeneratedAttachment(final Part part) throws MessagingException {
        return new GeneratedAttachmentRecogniser(part).isJiraGeneratedAttachment();
    }

    protected boolean shouldAttachMessagePart(final Part messagePart, final Message containingMessage)
            throws IOException, MessagingException
    {
        boolean keep = false;
        if (!shouldIgnoreEmailMessageAttachments()) {
            if (!isReplyMessagePart(messagePart, containingMessage)) {
                keep = !MailUtils.isContentEmpty(messagePart);
                if (!keep && log.isDebugEnabled()) {
                    log.debug("Attachment not attached to issue: Message is empty.");
                }
            } else {
                log.debug("Attachment not attached to issue: Detected as reply.");
            }
        } else {
            log.debug("Attachment not attached to issue: Message attachment has been disabled.");
        }
        return keep;
    }

    boolean shouldIgnoreEmailMessageAttachments() {
        return applicationProperties.getOption(APKeys.JIRA_OPTION_IGNORE_EMAIL_MESSAGE_ATTACHMENTS);
    }

    private boolean isReplyMessagePart(final Part messagePart, final Message containingMessage)
            throws IOException, MessagingException
    {
        boolean replyMessage;
        try {
            replyMessage = isMessageInReplyToAnother(containingMessage, (Message) messagePart.getContent());
        } catch (final ParseException e) {
            log.debug("Can't tell if the message is in reply to the attached message -- will attach it in case");
            replyMessage = false;
        }
        return replyMessage;
    }

    private boolean isMessageInReplyToAnother(final Message containingMessage, final Message attachedMessage)
            throws MessagingException, ParseException
    {
        final String attachMessageId = getMessageId(attachedMessage);
        final String[] replyToIds = containingMessage.getHeader(HEADER_IN_REPLY_TO);
        if (log.isDebugEnabled()) {
            log.debug("Checking if attachment was reply to containing message:");
            log.debug("\tAttachment mesage id: " + attachMessageId);
            log.debug("\tNew message reply to values: " + Arrays.toString(replyToIds));
        }
        if (replyToIds != null) {
            for (final String id : replyToIds) {
                if ((id != null) && id.equalsIgnoreCase(attachMessageId)) {
                    return true;
                }
            }
        }
        return false;
    }

    String getMessageId(final Message message)
    		throws MessagingException, ParseException
    {
        final String[] originalMessageIds = message.getHeader(HEADER_MESSAGE_ID);
        if ((originalMessageIds == null) || (originalMessageIds.length == 0)) {
            final String msg = "Could not retrieve Message-ID header from message: " + message;
            log.debug(msg);
            throw new ParseException(msg);
        }
        return originalMessageIds[0];
    }

    private boolean isPartAttachment(Part part)
    		throws MessagingException
    {
        return MailUtils.isPartAttachment(part) ||
    		(StringUtils.isNotBlank(part.getFileName())
				&& StringUtils.isNotBlank(part.getContentType())
				&& part.getSize() > 0);
    }

    protected boolean shouldAttachAttachmentsParts(final Part part)
    		throws MessagingException, IOException
    {
        return !MailUtils.isContentEmpty(part) && !MailUtils.isPartSignaturePKCS7(part);
    }

    protected boolean shouldAttachInlineParts(final Part part)
    		throws MessagingException, IOException
    {
        return !MailUtils.isContentEmpty(part)
    		&& !MailUtils.isPartSignaturePKCS7(part);
    }

    protected boolean shouldAttachRelatedPart(final Part part)
    		throws IOException, MessagingException
    {
        return !MailUtils.isContentEmpty(part);
    }

    // purpose of this one is only to collect attachments
    private void handleMultipart(final Multipart multipart, Message message, MessageHandlerContext context)
    		throws MessagingException, IOException
    {
        for (int i = 0, n = multipart.getCount(); i < n; i++) {
            if (log.isDebugEnabled()) {
                log.debug(String.format("Adding attachments for multi-part message. Part %d of %d.", i + 1, n));
            }
            final BodyPart part = multipart.getBodyPart(i);
            final Object partContent = part.getContent();
            if (partContent instanceof Multipart) {
                handleMultipart((Multipart) partContent, message, context);
            } else {
                handlePart(part, message, context);
            }
        }
    }

    private void handlePart(Part part, Message message, MessageHandlerContext context)
    		throws IOException, MessagingException {
    	prepareAttachmentWithPart(part, context, shouldAttach(part, message));
    }

    protected String getFilenameForAttachment(final Part part)
    		throws MessagingException, IOException
    {
        String filename = getFilenameFromPart(part);
        if (null == filename) {
            if (MailUtils.isPartMessageType(part)) {
                filename = getFilenameFromMessageSubject(part);
            } else if (MailUtils.isPartInline(part)) {
                filename = getFilenameFromContentType(part);
            }
        }
        if (null != filename) {
            if (StringUtils.isBlank(filename)) {
                log.warn("Filename should not be an empty string, but is...");
                filename = null;
            }
        }
        return filename;
    }

    private String getFilenameFromPart(final Part part)
    		throws MessagingException, IOException
    {
        String filename = part.getFileName();
        if (null != filename) {
            filename = MailUtils.fixMimeEncodedFilename(filename);
        }
        return filename;
    }

    private String getFilenameFromMessageSubject(final Part part)
    		throws MessagingException, IOException
    {
        final Message message = (Message) part.getContent();
        String filename = message.getSubject();
        if (StringUtils.isBlank(filename)) {
            try {
                filename = getMessageId(message);
            } catch (final ParseException e) {
                filename = ATTACHED_MESSAGE_FILENAME;
            }
        }
        return filename;
    }

    private String getFilenameFromContentType(final Part part)
    		throws MessagingException, IOException
    {
        String filename = DEFAULT_BINARY_FILE_NAME;
        final String contentType = MailUtils.getContentType(part);
        final int slash = contentType.indexOf("/");
        if (-1 != slash) {
            final String subMimeType = contentType.substring(slash + 1);
            // if its not a binary attachment convert the content type into a filename image/gif becomes image-file.gif
            if (!subMimeType.equals("bin")) {
                filename = contentType.substring(0, slash) + '.' + subMimeType;
            }
        }
        return filename;
    }

    protected void prepareAttachmentWithPart(final Part part, MessageHandlerContext context, boolean shouldAttach)
    		throws IOException
    {
        File file = null;
        try {
            final String contentType = MailUtils.getContentType(part);
            @SuppressWarnings("unused")
			final String rawFilename = part.getFileName();
            String fileName = getFilenameForAttachment(part);
            file = getFileFromPart(part);
            //fileName = renameFileIfInvalid(fileName, issue, reporter, context);
            if (log.isDebugEnabled()) {
                log.debug("part=" + part);
                log.debug("Filename=" + fileName + ", content type=" + contentType /*+ ", content=" + part.getContent() */ );
            }
            AttachmentWrapper aw = new AttachmentWrapper(file, fileName, contentType);
            if (!shouldAttach) {
            	aw.skip("Should not be attached");
            }
            preparedAttachments.add(aw);
        } catch (final Exception e) {
            throw new IOException(e.getMessage(), e);
        } finally {
        }
    }


    public Collection<ChangeItemBean> createPreparedAttachments(final ApplicationUser reporter,
    		final Issue issue)
    		throws IOException
    {
        boolean attachmentsAllowed =  attachmentsAllowed();
        boolean canCreateAttachments = canCreateAttachments(reporter,issue);

    	Collection<ChangeItemBean> attachmentChangeItems = Lists.newArrayList();
    	if (preparedAttachments != null) {
        	for(AttachmentWrapper aw : preparedAttachments) {
        		File file = aw.getFile();
        		String fileName = aw.getFileName();
        		String issueKey = (issue != null) ? issue.getKey() : "null";
				if (aw.shouldAttach()) {
					if (!attachmentsAllowed) {
						log.debug("Attachments not allowed on this instance");
					} else if (!canCreateAttachments) {
						log.debug("Reporter " + reporter + " does not have permissions to create attachments for " + issue);
					} else if (fileName == null) {
						log.debug("Refusing create attachement with null file name");
					} else {
						try {
							fileName = renameFileIfInvalid(fileName, issue, reporter, currentContext);
							final ChangeItemBean cib = currentContext.createAttachment(file, fileName, aw.getContentType(), reporter, issue);
				            if (cib != null) {
				                log.debug("Created attachment " + fileName + " for issue " + issueKey);
				                attachmentChangeItems.add(cib);
				            } else {
				                log.debug("Encountered an error creating the attachment " + fileName + " for issue " + issueKey);
				            }
						} catch (AttachmentException e) {
				            throw new IOException(e.getMessage(), e);
						}
					}
				} else {
	                log.debug("Skipping creating attachment " + fileName + " for issue " + issueKey + " : " + aw.getSkipReason());
				}
				aw.deleteFile();
        	}
    	}
    	clearPreparedAttachments();
    	return attachmentChangeItems;
    }

    protected File getFileFromPart(final Part part) throws IOException, MessagingException {
        final File tempFile;
        try {
            tempFile = createTempFile();
        } catch (IOException ex) {
            // Pretty bad - this means that the temp directory is not writable by JIRA
            throw new IOException("Unable to create a temporary file to copy attachment into.", ex);
        }
        final FileOutputStream out = new FileOutputStream(tempFile);
        try {
            part.getDataHandler().writeTo(out);
            return tempFile;
        } catch (IOException ex) {
            tempFile.delete();
            throw new IOException("Error while reading attachment, resulting file was incomplete and has been removed.", ex);
        } catch  (MessagingException ex) {
            tempFile.delete();
            throw ex;
        } finally {
            IOUtils.closeQuietly(out);
        }
    }

    public File createTempFile() throws IOException {
        return File.createTempFile("tempattach", "dat");
    }

    protected String renameFileIfInvalid(final String filename, final Issue issue, final ApplicationUser reporter,
            MessageHandlerContext context) {
        if (filename == null) {
            return null;
        }
        final String replacedFilename = fileNameCharacterCheckerUtil.replaceInvalidChars(filename, INVALID_CHAR_REPLACEMENT);
        if (!filename.equals(replacedFilename)) {
            if (log.isDebugEnabled()) {
                log.debug("Filename was invalid: replacing '" + filename + "' with '" + replacedFilename + "'");
            }
            String body = i18nHelper.getText("admin.renamed.file.cause.of.invalid.chars", filename, replacedFilename);
            context.createComment(issue, reporter, body, false);
            return replacedFilename;
        }
        return filename;

    }

    public void updateHistoryAndFireEvent(Collection<ChangeItemBean> attachmentsChangeItems,
    		Issue issue, ApplicationUser reporter, Comment comment, Long eventTypeId)
            throws JiraException
    {
    	if (eventTypeId == null) {
    		eventTypeId = getEventTypeId(attachmentsChangeItems, false);
    	}

    	if (log.isDebugEnabled()) {
    		log.debug("updateHistoryAndFireEvent: eventTypeId:" + eventTypeId.toString() +
    				", reporter:" + reporter.getName() + ", issue:" + issue.getKey());
    	}

        IssueUpdateBean issueUpdateBean = new IssueUpdateBean(issue, issue, eventTypeId, reporter);
        issueUpdateBean.setComment(comment);
        if (attachmentsChangeItems != null && !attachmentsChangeItems.isEmpty()) {
            issueUpdateBean.setChangeItems(attachmentsChangeItems);
        }
        issueUpdateBean.setDispatchEvent(true);
        issueUpdateBean.setParams(MapBuilder.<String, String>newBuilder().add("eventsource", IssueEventSource.ACTION).toMap());
        // do not let the issueUpdater generate change items
        issueUpdater.doUpdate(issueUpdateBean, false);
    }

    public static Long getEventTypeId(Collection<ChangeItemBean> attachmentsChangeItems, boolean forceFireCommented) {
        Long eventTypeId = EventType.ISSUE_COMMENTED_ID; // ISSUE_COMMENTED_ID = 6L;
        if (!forceFireCommented && attachmentsChangeItems != null && !attachmentsChangeItems.isEmpty()) {
            eventTypeId = EventType.ISSUE_UPDATED_ID; // ISSUE_UPDATED_ID = 2L;
        }
        return eventTypeId;
    }

    public Project getProject() {
    	String projectKey = params.get(PHPMailHandler.KEY_PROJECT_KEY);
        if (projectKey == null) {
        	String errorText = "Project key NOT set. Cannot find project.";
            log.warn(errorText);
            currentMonitor.warning(errorText);
            currentMonitor.messageRejected(currentMessage, errorText);
            return null;
        }
        if (log.isDebugEnabled()) {
            log.debug("Project key = " + projectKey);
        }
        Project project = projectManager.getProjectObjByKey(projectKey.toUpperCase(Locale.getDefault()));
        if (project == null) {
        	String errorText = "Project not configured";
        	log.warn(errorText);
        	currentMonitor.warning(errorText);
        	currentMonitor.messageRejected(currentMessage, errorText);
        }
        return project;
    }

    public IssueType getIssueTypeObjectByName(String name) {
    	IssueType issueType = null;
        Collection<IssueType> issueTypeObjects = constantsManager.getAllIssueTypeObjects();
        for(IssueType is: issueTypeObjects) {
        	if (is.getName().equals(name)) {
        		issueType = is;
        	}
        }
        if (issueType == null) {
        	String errText = "There's no such issue type: '" + name + "'";
        	currentMonitor.warning(errText);
        	log.error(errText);
        }
        return issueType;
    }

    public IssueType getIssueTypeObject() {
        // if there is no project then the issue cannot be created
        String issueType = params.get(PHPMailHandler.KEY_ISSUETYPE);
        if (issueType == null) {
        	String errText = "Issue Type NOT set";
        	currentMonitor.warning(errText);
            log.error(errText);
            return null;
        }
        IssueType issueTypeObject = getIssueTypeObjectByName(issueType);
        return issueTypeObject;
    }

    public String truncateSummary(String summary) {
        if (summary.length() > SummarySystemField.MAX_LEN.intValue())
        {
            currentMonitor.info("Truncating summary field because it is too long: " + summary);
            summary = summary.substring(0, SummarySystemField.MAX_LEN.intValue() - 3) + "...";
        }
        return summary;
    }

    public String getPriority(Message message, Project project, IssueType issueType) {
        String priority = null;
        if (!fieldVisibilityManager.isFieldHiddenInAllSchemes(project.getId(),
                IssueFieldConstants.PRIORITY, Collections.singletonList(issueType.getId())))
        {
            try {
				priority = getPriorityFromMessage(message);
			} catch (MessagingException e) {
				log.error("getPriorityFromMessage thrown exception: " + e.getMessage());
				return null;
			}
        }
        log.debug("Priority: " + priority);
        return priority;
    }

    public String getPriorityFromMessage(Message message) throws MessagingException {
        String[] xPrioHeaders = message.getHeader("X-Priority");

        if (xPrioHeaders != null && xPrioHeaders.length > 0) {
            String xPrioHeader = xPrioHeaders[0];

            int priorityValue = Integer.parseInt(TextUtils.extractNumber(xPrioHeader));

            if (priorityValue == 0) {
                return getDefaultSystemPriority();
            }

            // if priority is unset - pick the closest priority, this should be a sensible default
            Collection<Priority> priorities = constantsManager.getPriorityObjects();

            Iterator<Priority> priorityIt = priorities.iterator();

            /*
             * NOTE: Valid values for X-priority are (1=Highest, 2=High, 3=Normal, 4=Low & 5=Lowest) The X-Priority
             * (priority in email header) is divided by 5 (number of valid values) this gives the percentage
             * representation of the priority. We multiply this by the priority.size() (number of priorities in jira) to
             * scale and map the percentage to a priority in jira.
             */
            int priorityNumber = (int) Math.ceil((priorityValue / 5d) * priorities.size());
            // if priority is too large, assume its the 'lowest'
            if (priorityNumber > priorities.size()) {
                priorityNumber = priorities.size();
            }

            String priority = null;

            for (int i = 0; i < priorityNumber; i++) {
                priority = priorityIt.next().getId();
            }

            return priority;
        } else {
            return getDefaultSystemPriority();
        }
    }

    private String getDefaultSystemPriority() {
        // if priority header is not set, assume it's 'default'
        Priority defaultPriority = constantsManager.getDefaultPriorityObject();
        if (defaultPriority == null)
        {
            log.warn("Default priority was null. Using the 'middle' priority.");
            Collection<Priority> priorities = constantsManager.getPriorityObjects();
            final int times = (int) Math.ceil(priorities.size() / 2d);
            Iterator<Priority> priorityIt = priorities.iterator();
            for (int i = 0; i < times; i++) {
                defaultPriority = priorityIt.next();
            }
        }
        if (defaultPriority == null) {
            throw new RuntimeException("Default priority not found");
        }
        return defaultPriority.getId();
    }

    public String getDescription(Message message, Project project, IssueType issueType, ApplicationUser reporter) {
    	String description = null;
        if (!fieldVisibilityManager.isFieldHiddenInAllSchemes(project.getId(),
                IssueFieldConstants.DESCRIPTION, Collections.singletonList(issueType.getId())))
        {
            try {
				description = getDescriptionFromMessage(reporter, message);
			} catch (MessagingException e) {
				log.error("getDescription thrown exception: " + e.getMessage());
				return null;
			}
        }
        return description;

    }

    private String getDescriptionFromMessage(ApplicationUser reporter, Message message) throws MessagingException {
        return recordFromAddressForAnon(reporter, message, MailUtils.getBody(message));
    }

    public String getReplyTo(final Message message) throws MessagingException {
    	Address[] replyTo = message.getReplyTo();
        if ( replyTo != null && replyTo.length > 0) {
        	return replyTo[0].toString();
        }
        return null;
    }

    public String recordFromAddressForAnon(ApplicationUser reporter, Message message, String body) throws MessagingException {
        // If the message has been created for an anonymous user add the senders e-mail address to the description.
    	String reporteruserName = params.get(PHPMailHandler.KEY_REPORTER);
        if (reporteruserName != null && reporteruserName.equals(reporter.getName())) {
        	String description;
            description = "[Created via e-mail ";
            if (message.getFrom() != null && message.getFrom().length > 0) {
                description += "received from: " + message.getFrom()[0] + "]";
            } else {
                description += "but could not establish sender's address.]";
            }
            return description + "\n\n" + body;
        }
        return body;
    }

    public ApplicationUser getDefaultAssignee(Project project) {
    	  return projectManager.getDefaultAssignee(project, Collections.<ProjectComponent> emptySet());
    }

    public MutableIssue getNewIssueObject() {
    	return issueFactory.getIssue();
    }

    public void setDefaultSecurityLevel(MutableIssue issue) throws Exception {
        Project project = issue.getProjectObject();
        if (project != null) {
            final Long levelId = issueSecurityLevelManager.getDefaultSecurityLevel(project);
            if (levelId != null) {
                issue.setSecurityLevelId(levelId);
            }
        }
    }

    public void setCustomFieldDefaults(MutableIssue issue) {
    	log.warn("Setting defaults for customFields, issue: " + issue.getProjectId() + "/" + issue.getIssueTypeObject().getId());
        List<CustomField> customFieldObjects = customFieldManager.getCustomFieldObjects(issue);
        for (CustomField customField : customFieldObjects) {
        	Object value = customField.getDefaultValue(issue);
        	if (value != null) {
	        	log.warn("Setting defaults for customField:'" + customField.getFieldName() + "' value:"+value.toString());
        	} else {
	        	log.warn("Setting defaults for customField:'" + customField.getFieldName() + "' value is null");
        	}
            issue.setCustomFieldValue(customField, value);
        }
    }

    public void setCustomFieldValue(MutableIssue issue, String fieldId, Object value) {
    	CustomField customField = customFieldManager.getCustomFieldObject(fieldId);
    	if (customField == null)
    		customField = customFieldManager.getCustomFieldObjectByName(fieldId);
    	issue.setCustomFieldValue(customField, value);
    }

    public void addCcWatchersAndAttachments(Message message, Issue issue, ApplicationUser reporter) {
        try {
            if (issue != null) {
                // Add Cc'ed users as watchers if params set - JRA-9983
                if ("true".equals(params.get(PHPMailHandler.KEY_CC_WATCHER))) {
                    addCcWatchersToIssue(message, issue, reporter, currentContext, currentMonitor);
                }
                // Get the attachments out
                //createAttachmentsForMessage(message, issue, context);
                @SuppressWarnings("unused")
				Collection<ChangeItemBean> changeItems = createPreparedAttachments(reporter, issue);
            }
        } catch (AddressException ae) {
            addCommentIndicatingWatcherFailureAndMarkForDeletion(issue, currentContext);
        } catch (MessagingException me) {
            addCommentIndicatingAttachmentFailureAndMarkForDeletion(issue, currentContext, me);
        } catch (IOException ie) {
            addCommentIndicatingAttachmentFailureAndMarkForDeletion(issue, currentContext, ie);
        }
    }

    public void addCcWatchersToIssue(Message message, Issue issue, ApplicationUser reporter, MessageHandlerContext context, MessageHandlerExecutionMonitor messageHandlerExecutionMonitor) throws MessagingException
    {
        Collection<ApplicationUser> users = getAllUsersFromEmails(message.getAllRecipients());
        log.debug("addCcWatchersToIssue: " + users.toString());
        // we don't want to add the reporter to the watchers list, lets get rid of him.
        users.remove(reporter);
        if (!users.isEmpty()) {
            if (context.isRealRun()) {
                for (ApplicationUser user : users) {
                	watcherManager.startWatching(user, issue);
                }
            } else {
                final String watchers = StringUtils.join(users, ",");
                messageHandlerExecutionMonitor.info("Adding watchers " + watchers);
                log.debug("Watchers [" + watchers + "] not added due to dry-run mode.");
            }
        }
    }

    public Collection<ApplicationUser> getAllUsersFromEmails(Address addresses[])
    {
        if (addresses == null || addresses.length == 0) {
            return Collections.emptyList();
        }
        final List<ApplicationUser> users = new ArrayList<ApplicationUser>();
        for (Address address : addresses) {
            String emailAddress = getEmailAddress(address);
            if (emailAddress != null) {
            	ApplicationUser user = UserUtils.getUserByEmail(emailAddress);
                if (user != null) {
                    users.add(user);
                }
            }
        }
        return users;
    }

    private String getEmailAddress(Address address) {
        if (address instanceof InternetAddress) {
            return ((InternetAddress) address).getAddress();
        }
        return null;
    }

    private void addCommentIndicatingWatcherFailureAndMarkForDeletion(Issue issue, MessageHandlerContext context)
    {
        context.createComment(issue, null, i18nHelper.getText("jmp.handler.watchers.failed"), true);
        context.getMonitor().markMessageForDeletion(i18nHelper.getText("jmp.handler.watchers.failed"));
    }

    protected void addCommentIndicatingAttachmentFailureAndMarkForDeletion(Issue issue, MessageHandlerContext context, Exception e)
    {
        log.error("Exception while adding attachments to " + issue.getKey() +
                ". Some attachments from the message might be missing. Making a note on the issue.", e);
        context.createComment(issue, null, i18nHelper.getText("jmp.handler.attachments.failed"), true);
        context.getMonitor().markMessageForDeletion(i18nHelper.getText("jmp.handler.attachments.failed"));
    }

    public ApplicationUser getFirstValidAssignee(Address[] addresses, Project project) {
        if (addresses == null || addresses.length == 0) {
        	log.debug("getFirstValidAssignee: empty address list");
            return null;
        }
        for (Address address : addresses) {
            if (address instanceof InternetAddress) {
                InternetAddress email = (InternetAddress) address;
                String addr = email.getAddress();
            	log.debug("getFirstValidAssignee: looking for "+addr);
            	ApplicationUser validUser = UserUtils.getUserByEmail(addr);
                if (validUser != null) {
                	log.debug("getFirstValidAssignee: found:" + validUser.getName());
                }
                if (validUser != null && isAssignable(validUser, project)) {
                	log.debug("getFirstValidAssignee: returning " + validUser.getName());
                    return validUser;
                }
            }
        }
    	log.debug("getFirstValidAssignee: found no valid user");
        return null;
    }


    public WatcherManager getWatcherManager() {
    	return watcherManager;
    }

    public IssueSecurityLevelManager getIssueSecurityLevelManager() {
        return issueSecurityLevelManager;
    }

    public CustomFieldManager getCustomFieldManager() {
    	return customFieldManager;
    }

    public FieldVisibilityManager getFieldVisibilityManager() {
        return fieldVisibilityManager;
    }

    public ConstantsManager getConstantsManager() {
        return constantsManager;
    }

    public IssueFactory getIssueFactory() {
        return issueFactory;
    }

    public ProjectManager getProjectManager() {
        return projectManager;
    }

    public I18nHelper getI18nHelper() {
        return i18nHelper;
    }

    public MailLoopDetectionService getMailLoopDetectionService() {
        return mailLoopDetectionService;
    }

	public void setParams(Map<String, String> params) {
		this.params = params;
	}

/*
    private interface AttachmentHandler {
        void handlePart(Part part, Message containingMessage) throws IOException, MessagingException;
        void summarize();
    }

*/
	public class AttachmentWrapper {
		private File file;
    	private String fileName;
    	private String contentType;
    	private String md5;
    	private String skipReason;
		public AttachmentWrapper(File file, String fileName, String contentType) {
			this.file = file;
			this.fileName = fileName;
			this.contentType = contentType;
			this.skipReason = null;
			setMd5();
			log.debug("Prepared attachment: "  + md5  + ", " + fileName + ", " + contentType + " size:" + file.length());
		}
    	public void deleteFile() {
    		if (file != null) {
    			log.debug("Deleting temporary file " + file.getPath() + " for attachment " + fileName);
    			file.delete();
    			file = null;
    		}
		}
		public File getFile() {
			return file;
		}
		public void setFile(File file) {
			this.file = file;
		}
		public String getFileName() {
			return fileName;
		}
		public void setFileName(String fileName) {
			this.fileName = fileName;
		}
		public String getContentType() {
			return contentType;
		}
		public void setContentType(String contentType) {
			this.contentType = contentType;
		}
		public String getMd5() {
			return md5;
		}
		public void setMd5() {
			if (file != null)
				md5 = PHPRunnerUtils.md5(file);
		}
		public boolean shouldAttach() {
			return (skipReason == null);
		}
		public String getSkipReason() {
			return skipReason;
		}
		public void skip(String reason) {
			this.skipReason = reason;
		}
		public void skip() {
			this.skipReason = null;
		}
    }

}
