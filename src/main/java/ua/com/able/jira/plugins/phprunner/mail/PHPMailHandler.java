package ua.com.able.jira.plugins.phprunner.mail;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.Map;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.SimpleBindings;

import org.apache.log4j.Logger;

import com.atlassian.jira.JiraApplicationContext;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.security.IssueSecurityLevelManager;
import com.atlassian.jira.service.util.handler.MessageHandler;
import com.atlassian.jira.service.util.handler.MessageHandlerContext;
import com.atlassian.jira.service.util.handler.MessageHandlerErrorCollector;
import com.atlassian.jira.service.util.handler.MessageUserProcessor;
import com.atlassian.jira.user.UserKeyService;
import com.atlassian.jira.util.I18nHelper;
import com.caucho.quercus.env.Value;
import com.caucho.quercus.script.QuercusScriptEngine;

import ua.com.able.jira.plugins.phprunner.quercus.ScriptingManager;
import ua.com.able.jira.plugins.phprunner.utils.PHPRunnerUtils;

import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.PermissionManager;

import com.google.common.collect.Lists;


import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.util.IssueUpdater;
import com.atlassian.jira.issue.watchers.WatcherManager;


public class PHPMailHandler implements MessageHandler {

	//private final IssueKeyValidator issueKeyValidator;
	protected static Logger log = Logger.getLogger(PHPMailHandler.class);
	private static ScriptingManager scriptingManager;
	private static UserKeyService userKeyService;

	private Map<String, String> params;
	private MailHandler mailHandler;

	public static final String KEY_INLINE_SCRIPT = "inlineSrc";
	public static final String KEY_FILE_PATH = "fileSrc";
	public static final String KEY_REPORTER = "reporterUserName";
    public static final String KEY_REPORTER_KEY = "reporterUserKey";
    public static final String KEY_CATCHEMAIL = "catchemail";
    public static final String KEY_FORWARD_EMAIL = "forwardEmail";
    //public static final String KEY_CREATEUSERS = "createUsers";
    //public static final String KEY_NOTIFYUSERS = "notifyUsers";
    public static final String KEY_CC_ASSIGNEE = "ccassignee";
    public static final String KEY_CC_WATCHER = "ccwatcher";
    public static final String KEY_PROJECT_KEY = "projectKey";
    public static final String KEY_ISSUETYPE = "issueType";
    public static final String KEY_STRIP_QUOTES = "stripquotes";
    public static final String KEY_FINGER_PRINT_POLICY = "fingerprintPolicy";
    public static final String KEY_FINGER_PRINT = "fingerprint";
    public static final String VALUE_FINGER_PRINT_ACCEPT = "accept";
    public static final String VALUE_FINGER_PRINT_FORWARD = "forward";
    public static final String VALUE_FINGER_PRINT_IGNORE = "ignore";
    public static final List<String> VALUES_FINGERPRINT = Lists.newArrayList(VALUE_FINGER_PRINT_ACCEPT, VALUE_FINGER_PRINT_FORWARD, VALUE_FINGER_PRINT_IGNORE);
    public static final String KEY_BULK = "bulk";
    //the 4 possible values of bulk key. (default in other cases)
    public static final String VALUE_BULK_ACCEPT = "accept";
    public static final String VALUE_BULK_IGNORE = "ignore";
    public static final String VALUE_BULK_FORWARD = "forward";
    public static final String VALUE_BULK_DELETE = "delete";


	public PHPMailHandler(final MessageUserProcessor messageUserProcessor,
			final PermissionManager permissionManager, ApplicationProperties applicationProperties,
			final IssueUpdater issueUpdater, final WatcherManager watcherManager,
			final IssueSecurityLevelManager issueSecurityLevelManager, final CustomFieldManager customFieldManager,
			final ConstantsManager constantsManager, final IssueFactory issueFactory,
			final ProjectManager projectManager, final I18nHelper i18nHelper,
			final JiraApplicationContext jiraApplicationContext, UserKeyService __userKeyService, final ScriptingManager __scriptingManager)
	{
		scriptingManager = __scriptingManager;
        userKeyService = __userKeyService;
        mailHandler = new MailHandler(messageUserProcessor,
    			permissionManager, applicationProperties,
    			issueUpdater, watcherManager,
    			issueSecurityLevelManager, customFieldManager,
    			constantsManager, issueFactory,
    			projectManager, i18nHelper,
    			jiraApplicationContext);
        log.debug("Constructed handler: " + this.toString());
	}

	@Override
	public boolean handleMessage(Message message, MessageHandlerContext context) throws MessagingException {
        //log.debug("handler: " + this.toString());
		if (log.isDebugEnabled()) {
			log.debug("handleMessage() params: " + params);
		}
		StringWriter stdoutWriter = new StringWriter();
		StringWriter stderrWriter = new StringWriter();
		Value value = null;
	    Bindings bindings = new SimpleBindings();
	    String filePath = params.get(KEY_FILE_PATH);
		try {
			QuercusScriptEngine quercusScriptEngine = scriptingManager.getQuercusScriptEngine();
			mailHandler.setCurrentContext(message, context);
		    bindings.put("log", log);
		    bindings.put("params", this.params);
		    bindings.put("handler", mailHandler);
		    bindings.put("message", message);
		    bindings.put("context", context);
		    bindings.put("monitor", context.getMonitor());
		    quercusScriptEngine.getContext().setWriter(stdoutWriter);
		    quercusScriptEngine.getContext().setErrorWriter(stderrWriter);
		    quercusScriptEngine.getContext().setBindings(bindings, ScriptContext.ENGINE_SCOPE);
		    value = scriptingManager.eval(null, filePath, quercusScriptEngine.getContext());
		} catch (Exception ex) {
			log.error("Exception while executing PHP script: " + ex.getMessage(), ex);
		}
		stdoutWriter.flush();
		stderrWriter.flush();

		if (log.isDebugEnabled()) {
			log.debug("stdout: "  + stdoutWriter.toString());
			log.debug("stderr: "  + stderrWriter.toString());
		}

		mailHandler.clearPreparedAttachments(); // for any case
		// message will be forwarded if ((monitor.hasErrors()&&!delete)||monitor.isMessagedMarkedForDeletion())
		// message will be deleted if (delete||monitor.isMessagedMarkedForDeletion())
		boolean deleteIt = (value == null) ? false : value.toBoolean();
		return deleteIt;
	}

	@Override
	public void init(Map<String, String> params, MessageHandlerErrorCollector monitor) {
		processConfigParams(params);
        this.params = params;
        mailHandler.setParams(params);
        log.warn("init() final params: " + params);
	}

	public static void processConfigParams(Map<String, String> parameters) {
        if (parameters.containsKey(KEY_INLINE_SCRIPT)) {
            String inlineScript = parameters.get(KEY_INLINE_SCRIPT);
        	PrintWriter stdoutWriter = new PrintWriter(System.out, true);
        	PrintWriter stderrWriter = new PrintWriter(System.err, true);
    	    @SuppressWarnings("unused")
    		Value value = null;
		    Bindings bindings = new SimpleBindings();
    		try {
    			QuercusScriptEngine quercusScriptEngine = scriptingManager.getQuercusScriptEngine();
    		    bindings.put("log", log);
    		    bindings.put("params", parameters);
    		    quercusScriptEngine.getContext().setWriter(stdoutWriter);
    		    quercusScriptEngine.getContext().setErrorWriter(stderrWriter);
    		    quercusScriptEngine.getContext().setBindings(bindings, ScriptContext.ENGINE_SCOPE);
    		    String saveConfigScript = PHPRunnerUtils.readResource("/templates/phprunner/admin/savephpmailhandlerconfig.php.txt");
    		    value = scriptingManager.eval(inlineScript+saveConfigScript, null, quercusScriptEngine.getContext());
    		} catch (Exception ex) {
    			log.error("Exception while executing PHP script: " + ex.getMessage(), ex);
    		}
    		stdoutWriter.flush();
    		stderrWriter.flush();

            if (parameters.containsKey(KEY_REPORTER)) {
                String reporteruserName = parameters.get(KEY_REPORTER);
                parameters.put(KEY_REPORTER_KEY, userKeyService.getKeyForUsername(reporteruserName));
            } else if (parameters.containsKey(KEY_REPORTER_KEY)) {
    			String reporterUserKey = parameters.get(KEY_REPORTER_KEY);
    			parameters.put(KEY_REPORTER, userKeyService.getUsernameForKey(reporterUserKey));
            }
            if (parameters.get(KEY_FINGER_PRINT) != null && VALUES_FINGERPRINT.contains(parameters.get(KEY_FINGER_PRINT))) {
            	parameters.put(KEY_FINGER_PRINT_POLICY, parameters.get(KEY_FINGER_PRINT));
            } else {
                log.debug("Defaulting to fingerprint policy of 'forward'");
                parameters.put(KEY_FINGER_PRINT_POLICY, VALUE_FINGER_PRINT_FORWARD);
            }
        }
	}

}
