package ua.com.able.jira.plugins.phprunner.workflow;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.google.common.base.Strings;
import com.opensymphony.workflow.loader.AbstractDescriptor;
import com.opensymphony.workflow.loader.ConditionDescriptor;
import com.opensymphony.workflow.loader.FunctionDescriptor;
import com.opensymphony.workflow.loader.ValidatorDescriptor;

public abstract class AbstractPHPWorkflowPluginFactory extends AbstractWorkflowPluginFactory {

	protected final Logger log = Logger.getLogger(this.getClass());
	private final PageBuilderService pageBuilderService;

	public static final String SOURCE_FILE = "fileSrc";
	public static final String SOURCE_INLINE = "inlineSrc";

	public AbstractPHPWorkflowPluginFactory(final PageBuilderService pageBuilderService) {
        this.pageBuilderService = pageBuilderService;
	}


	@Override
	public Map<String, ?> getDescriptorParams(Map<String, Object> formParams) {
		log.warn("getDescriptorParams: " + formParams);
		Map<String,String> map = new HashMap<String, String>();
		map.put(SOURCE_INLINE, getStringParam(formParams, SOURCE_INLINE));
		map.put(SOURCE_FILE, getStringParam(formParams, SOURCE_FILE).trim());
		//log.warn("getDescriptorParams: " + map);
		return map;
	}

	@Override
	protected void getVelocityParamsForEdit(Map<String, Object> velocityParams,	AbstractDescriptor descriptor) {
		requireResources();
		Map<String, Object> args = getDescriptorArgs(descriptor);
		velocityParams.putAll(args);
		//getVelocityParamsForInput(velocityParams);
		//log.warn("getVelocityParamsForEdit: " + velocityParams);
	}

	@Override
	protected void getVelocityParamsForInput(Map<String, Object> velocityParams) {
		requireResources();
		// really nothing to put for a new stuff
		// TODO: probably put a list of existing scripts
		//log.warn("getVelocityParamsForInput: " + velocityParams);
	}

	@Override
	protected void getVelocityParamsForView(Map<String, Object> velocityParams, AbstractDescriptor descriptor) {
		Map<String, Object> args = getDescriptorArgs(descriptor);
		velocityParams.putAll(args);
		//log.warn("getVelocityParamsForView: " + velocityParams);
	}


	@SuppressWarnings("unchecked")
	private Map<String, Object> getDescriptorArgs(AbstractDescriptor descriptor) {
		Map<String, Object> args;
		if (descriptor instanceof ValidatorDescriptor)
			args = ((ValidatorDescriptor) descriptor).getArgs();
		else if (descriptor instanceof ConditionDescriptor)
			args = ((ConditionDescriptor) descriptor).getArgs();
		else if (descriptor instanceof FunctionDescriptor)
			args = ((FunctionDescriptor) descriptor).getArgs();
		else
			args = new HashMap<String, Object>();
		//log.warn("getDescriptorArgs: " + args);
		return args;
	}

	private String getStringParam(Map<String, Object> params, String key) {
		//log.warn("getStringParam: " + key + ": " + params);
		return Strings.nullToEmpty(extractSingleParam(params, key));
	}

	private void requireResources() {
    	pageBuilderService.assembler().resources()
			.requireWebResource("ua.com.able.jira.plugins.phprunner:phprunner-resources")
			.requireWebResource("ua.com.able.jira.plugins.phprunner:codemirror");
	}

}
