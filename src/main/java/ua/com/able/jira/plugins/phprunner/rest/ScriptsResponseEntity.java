package ua.com.able.jira.plugins.phprunner.rest;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ScriptsResponseEntity {

	@XmlElement
	private String[] scripts;

    public ScriptsResponseEntity(String[] scripts) {
		this.scripts = scripts;
	}

    public ScriptsResponseEntity() {
	}

	public String[] getScripts() {
		return scripts;
	}

	public void setScripts(String[] scripts) {
		this.scripts = scripts;
	}

}
