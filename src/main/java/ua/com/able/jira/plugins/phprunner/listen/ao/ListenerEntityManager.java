package ua.com.able.jira.plugins.phprunner.listen.ao;

import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.atlassian.activeobjects.external.ActiveObjects;

import net.java.ao.EntityManager;
import net.java.ao.RawEntity;
import ua.com.able.jira.plugins.phprunner.utils.PHPRunnerUtils;

public class ListenerEntityManager {

	private ActiveObjects entityManager;

	// imitates entity for creation of one without being created in database
	public class FakeListenerEntity implements ListenerEntity {
		private int ID;
		private String description;
		private String projects;
		private String events;
		private String inlineScript;
		private String filePath;
		@Override
		public int getID() {
			return ID;
		}
		public void setID(int ID) {
			this.ID = ID;
		}
		@Override
		public void init() {
		}
		@Override
		public void save() {
		}
		@Override
		public EntityManager getEntityManager() {
			return null;
		}
		@Override
		public <X extends RawEntity<Integer>> Class<X> getEntityType() {
			return null;
		}
		@Override
		public void addPropertyChangeListener(PropertyChangeListener listener) {
		}
		@Override
		public void removePropertyChangeListener(PropertyChangeListener listener) {
		}
		@Override
		public String getInlineScript() {
			return inlineScript;
		}
		@Override
		public String getProjects() {
			return projects;
		}
		@Override
		public String getEvents() {
			return events;
		}
		@Override
		public String getFilePath() {
			return filePath;
		}
		@Override
		public String getDescription() {
			return description;
		}
		@Override
		public void setInlineScript(String inlineScript) {
			this.inlineScript = inlineScript;
		}
		@Override
		public void setFilePath(String filePath) {
			this.filePath = filePath;
		}
		@Override
		public void setProjects(String projects) {
			this.projects = projects;
		}
		@Override
		public void setEvents(String events) {
			this.events = events;
		}
		@Override
		public void setDescription(String description) {
			this.description = description;
		}
	}

	public ListenerEntityManager(final ActiveObjects entityManager) {

		this.entityManager = entityManager;
	}

	public ListenerEntity getNewEntity() {
		return new FakeListenerEntity();
	}

	public ListenerEntity getEntityById(Integer id) {
		return entityManager.get(ListenerEntity.class, id);
	}

	public void deleteEntityById(Integer id) {
		ListenerEntity entity = entityManager.get(ListenerEntity.class, id);
		if (entity != null)
			entityManager.delete(entity);
	}

	public void createAndSave(ListenerEntity entityObject) {
		ListenerEntity entity = entityManager.create(ListenerEntity.class);
		entity.setDescription(entityObject.getDescription());
		entity.setProjects(entityObject.getProjects());
		entity.setEvents(entityObject.getEvents());
		entity.setInlineScript(entityObject.getInlineScript());
		entity.setFilePath(entityObject.getFilePath());
		entity.save();
	}

	public ListenerEntity[] getEntities() {
		return entityManager.find(ListenerEntity.class);
	}

	public static String convertEventTypesToString(String[] types) {
		StringBuffer result = new StringBuffer();
		for (String t: types) {
			result.append(t);
			result.append(',');
		}
		if (result.length()>0)
			result.setLength(result.length()-1);
		return result.toString();
	}

	public static String convertProjectsToString(String[] projects) {
		StringBuffer result = new StringBuffer();
		for (String p: projects) {
			result.append(p);
			result.append(',');
		}
		if (result.length()>0)
			result.setLength(result.length()-1);
		return result.toString();
	}

	public List<ListenerEntity> getRelevantListeners(String projectId, String eventTypeId) {
		boolean isIssueEvent = PHPRunnerUtils.isLong(eventTypeId);
		List<ListenerEntity> entities = new ArrayList<ListenerEntity>();
		for(ListenerEntity e: entityManager.find(ListenerEntity.class)) {
			List<String> eventIds = Arrays.asList(e.getEvents().split(","));
			List<String> projIds = Arrays.asList(e.getProjects().split(","));

			if (projIds.contains("") || projIds.contains(projectId)) {
				if ((isIssueEvent && eventIds.contains("")) || eventIds.contains(eventTypeId)) {
					entities.add(e);
				}
			}
		}
		return entities;
	}

}
