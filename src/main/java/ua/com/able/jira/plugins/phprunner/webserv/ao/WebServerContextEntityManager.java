package ua.com.able.jira.plugins.phprunner.webserv.ao;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.activeobjects.external.ActiveObjects;

import net.java.ao.DBParam;
import net.java.ao.Query;

public class WebServerContextEntityManager {
	private ActiveObjects entityManager;
	private static final Logger log = LoggerFactory.getLogger(WebServerContextEntityManager.class);

	public WebServerContextEntityManager(final ActiveObjects entityManager) {
		this.entityManager = entityManager;
	}

	public WebServerContextEntity getById(Integer id) {
		if (id == null)
			return null;
		return entityManager.get(WebServerContextEntity.class, id);
		//return null;
	}

	public WebServerContextEntity getByContext(String context) {
		WebServerContextEntity[] res = entityManager.find(WebServerContextEntity.class, Query.select().where("CONTEXT = ?", context));
		if (res.length == 0)
			return null;
		return res[0];
	}

	public WebServerContextEntity[] getAll() {
		return entityManager.find(WebServerContextEntity.class);
	}

	public void deleteById(Integer id) {
		WebServerContextEntity e = getById(id);
		if (e != null) {
			entityManager.delete(e);
		}
	}

	public WebServerContextEntity create(Map<String,Object> map) {
		return create((String)map.get("context"),
				(String) map.get("documentRoot"),
				"true".equals(map.get("requiresAuthenticatedUser")),
				"true".equals(map.get("requiresWebSudo")));
	}

	public void update(Map<String,Object> map) {
		update( ((Integer) map.get("id")).intValue(),
				(String)map.get("context"),
				(String) map.get("documentRoot"),
				"true".equals(map.get("requiresAuthenticatedUser")),
				"true".equals(map.get("requiresWebSudo")));
	}

	public void update(int id, String context, String documentRoot,
			boolean requiresAuthenticatedUser, boolean requiresWebSudo)
	{
		log.debug("updating entity: "+id+" '"+context+"' '"+documentRoot+"' " + requiresAuthenticatedUser + " " + requiresWebSudo);
		WebServerContextEntity e = getById(id);
		if (e == null) {
			log.error("entity does not exist! id: "+id);
			return;
		}
		e.setContext(context);
		e.setDocumentRoot(documentRoot);
		e.setRequiresAuthenticatedUser(requiresAuthenticatedUser);
		e.setRequiresWebSudo(requiresWebSudo);
		e.save();
	}

	public WebServerContextEntity create(String context, String documentRoot,
			boolean requiresAuthenticatedUser, boolean requiresWebSudo)
	{
		log.debug("creating entity: '"+context+"' '"+documentRoot+"' " + requiresAuthenticatedUser + " " + requiresWebSudo);
		WebServerContextEntity e = getByContext(context);
		if (e != null)
			return null;
		e = entityManager.create(
				WebServerContextEntity.class,
		        new DBParam("CONTEXT", context),
		        new DBParam("DOCUMENT_ROOT", documentRoot)
		    );
		e.setRequiresAuthenticatedUser(requiresAuthenticatedUser);
		e.setRequiresWebSudo(requiresWebSudo);
		e.save();
		return e;
	}

	public void delete(int id) {
		WebServerContextEntity e = getById(id);
		if (e == null) {
			log.error("entity does not exist! id: "+id);
			return;
		}
		entityManager.delete(e);
	}

}
