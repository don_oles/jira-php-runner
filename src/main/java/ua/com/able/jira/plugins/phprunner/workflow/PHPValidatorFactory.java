package ua.com.able.jira.plugins.phprunner.workflow;

import com.atlassian.jira.plugin.workflow.WorkflowPluginValidatorFactory;
import com.atlassian.webresource.api.assembler.PageBuilderService;

public class PHPValidatorFactory extends AbstractPHPWorkflowPluginFactory implements WorkflowPluginValidatorFactory {

	public PHPValidatorFactory(final PageBuilderService pageBuilderService) {
		super(pageBuilderService);
	}

}
