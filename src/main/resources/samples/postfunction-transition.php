<?php

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.util.ImportUtils;
import com.atlassian.jira.issue.IssueInputParametersImpl;

if (!$inputParameters)
    $inputParameters = [];
$issueInputParameters = new IssueInputParametersImpl($inputParameters);

if (!$targetUser)
    $targetUser = $currentUser;

$log->debug("transitionning ${targetIssue} as ${targetUser}, action: ${actionId}, inputParameters:${inputParameters}");

$indexManager = jira_component("com.atlassian.jira.issue.index.IssueIndexManager");
$issueService = ComponentAccessor::getIssueService();

$validationResult = $issueService->validateTransition($targetUser, $targetIssue->id, $actionId, $issueInputParameters);
$isValid = $validationResult->isValid();
$errorCollection = $validationResult->errorCollection;

if (!$isValid) {
    $log->error("Transition not validated, errorCollection:${errorCollection}");
    return;
}

$errorCollection = $issueService->transition($targetUser, $validationResult)->errorCollection;

if ($errorCollection->hasAnyErrors()) {
    $log->error("Transition failed, errorCollection:${errorCollection}");
    return;
}

// reload issue
$targetIssue = ComponentAccessor::getIssueManager()->getIssueObject($targetIssue->id);
$wasIndexing = ImportUtils::isIndexIssues();
$indexManager->reIndex($targetIssue);
ImportUtils::setIndexIssues($wasIndexing);

?>