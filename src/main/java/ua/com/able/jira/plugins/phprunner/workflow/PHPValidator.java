package ua.com.able.jira.plugins.phprunner.workflow;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.SimpleBindings;

import org.apache.log4j.Logger;

import com.atlassian.jira.workflow.WorkflowFunctionUtils;
import com.caucho.quercus.env.Value;
import com.caucho.quercus.script.QuercusScriptEngine;
import com.google.common.base.Strings;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;

import ua.com.able.jira.plugins.phprunner.quercus.ScriptingManager;

public class PHPValidator implements Validator {

	private static Logger log = Logger.getLogger(PHPValidator.class);
	private final ScriptingManager scriptingManager;

	public PHPValidator(final ScriptingManager scriptingManager) {
		this.scriptingManager = scriptingManager;
	}

	@Override
	public void validate(@SuppressWarnings("rawtypes") Map transientVars,
			@SuppressWarnings("rawtypes") Map args, PropertySet ps)
					throws InvalidInputException, WorkflowException
	{

		InvalidInputException invalidEx = null;

//    	PrintWriter stdoutWriter = new PrintWriter(System.out, true);
    	PrintWriter stderrWriter = new PrintWriter(System.err, true);
		StringWriter stdoutWriter = new StringWriter();
//		StringWriter stderrWriter = new StringWriter();

    	@SuppressWarnings("unused")
		Value value = null;

	    try {
			QuercusScriptEngine quercusScriptEngine = scriptingManager.getQuercusScriptEngine();
		    Bindings bindings = new SimpleBindings();

		    HashMap<String,String> errors = new HashMap<String,String>();

		    bindings.put("log", log);
		    bindings.put("transientVars", transientVars);
		    bindings.put("propertySet", ps);
		    bindings.put("args", args);
		    bindings.put("issue", transientVars.get("issue"));
		    bindings.put("currentUser", WorkflowFunctionUtils.getCallerUserFromArgs(transientVars, args));
		    bindings.put("originalIssue", transientVars.get("originalissueobject"));
		    bindings.put("errors", errors);

		    quercusScriptEngine.getContext().setWriter(stdoutWriter);
		    quercusScriptEngine.getContext().setErrorWriter(stderrWriter);
		    quercusScriptEngine.getContext().setBindings(bindings, ScriptContext.ENGINE_SCOPE);

		    String inlineScript = Strings.nullToEmpty((String)args.get(AbstractPHPWorkflowPluginFactory.SOURCE_INLINE));
		    String fileScript = Strings.nullToEmpty((String)args.get(AbstractPHPWorkflowPluginFactory.SOURCE_FILE));

		    value = scriptingManager.eval(inlineScript,fileScript,quercusScriptEngine.getContext());
//		    value = scriptingManager.eval(inlineScript,fileScript);

		    if (errors.size() != 0) {
		    	invalidEx = new InvalidInputException(errors.get(""));
		    	for (String fieldKey: errors.keySet()) {
		    		if (fieldKey != "")
		    			invalidEx.addError(fieldKey, errors.get(fieldKey));
		    	}
		    }

		} catch (Exception ex) {
			log.error("Unrecognized exception while executing PHP script: " + ex.getMessage(), ex);
			throw new WorkflowException(ex);
		}

		stdoutWriter.flush();
		stderrWriter.flush();

		if (log.isDebugEnabled()) {
			log.debug("stdout: "  + stdoutWriter.toString());
		}

		if (invalidEx != null) {
			throw invalidEx;
		}

	}

}
