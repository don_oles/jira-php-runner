package ua.com.able.jira.plugins.phprunner.workflow;

import com.atlassian.jira.plugin.workflow.WorkflowPluginFunctionFactory;
import com.atlassian.webresource.api.assembler.PageBuilderService;

public class PHPPostFunctionFactory extends AbstractPHPWorkflowPluginFactory implements WorkflowPluginFunctionFactory {

	public PHPPostFunctionFactory(final PageBuilderService pageBuilderService) {
		super(pageBuilderService);
	}

}
