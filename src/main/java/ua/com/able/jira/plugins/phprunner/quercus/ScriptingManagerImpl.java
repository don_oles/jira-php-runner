package ua.com.able.jira.plugins.phprunner.quercus;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.script.CompiledScript;
import javax.script.ScriptContext;
import javax.script.ScriptException;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import com.atlassian.sal.api.component.ComponentLocator;
import com.caucho.quercus.QuercusContext;
import com.caucho.quercus.env.Value;
import com.caucho.quercus.module.ModuleContext;
import com.caucho.quercus.module.ModuleInfo;
import com.caucho.quercus.script.QuercusScriptEngine;
import com.caucho.vfs.Path;
import com.caucho.vfs.Vfs;
import com.google.common.base.Strings;

import ua.com.able.jira.plugins.phprunner.utils.PHPRunnerUtils;
import ua.com.able.jira.plugins.phprunner.utils.StackMap;

public class ScriptingManagerImpl implements ScriptingManager {

	private static Logger log = Logger.getLogger(ScriptingManager.class);

	private static QuercusScriptEngine quercusScriptEngine;
	private static StringWriter writer;
	private final JiraModule jiraModule;

	private static StackMap<String, CompiledScript> _scriptCache;
	private static long _totalCompileTime = 0;
	private static long _totalEvalTime = 0;
	private static boolean _cachingEnabled = true;
	private static int _cacheSize = 10;
	private static int _evals = 0;

	public ScriptingManagerImpl(final JiraModule jiraModule) {
		this.jiraModule = jiraModule;
		_scriptCache = new StackMap<String, CompiledScript>();
	}

	@Override
	public String getScriptRoot() {
		return PHPRunnerUtils.getScriptRoot();
	}

	@Override
	public void init() {
		StringBuffer sb = new StringBuffer();
		QuercusContext quercus = quercusScriptEngine.getQuercus();
		Path iniPath = Vfs.lookup(getScriptRoot() + "/php.ini");
		quercus.setIniFile(iniPath);
		sb.append("=== php.ini : ");
		sb.append(quercus.getIniFile().getPath());
		sb.append("\n");

		quercus.init();
		try {
			quercusScriptEngine
					.eval("<?php echo 'Hello JIRA! I am PHP script. Testing php.ini: ' . ini_get('test_php_ini'); ?>");
		} catch (ScriptException e) {
			e.printStackTrace();
		}
		sb.append("=== Testing PHP: ");
		sb.append(getOutput());
		sb.append("\n");
		log.warn(sb.toString());

		_cachingEnabled = !quercus.getIniBoolean("phprunner_disable_script_caching");
		if (_cachingEnabled) {
			log.warn("Script caching enabled");
		} else {
			log.warn("Script caching disabled");
		}
		long cacheSize = quercus.getIniLong("phprunner_cache_size");
		if (cacheSize > 0L && cacheSize <= 10000L) {
			_cacheSize = (int) cacheSize;
		}
		log.warn("Cache size: " + _cacheSize);
	}

	@Override
	public void create() {
		if (quercusScriptEngine != null)
			return;
		createQuercusScriptEngine();
		fixQuercusClassLoader();
	}

	private void createQuercusScriptEngine() {

		// just for any case
		if (quercusScriptEngine != null) {
			return;
		}

		try {

			StringBuffer sb = new StringBuffer();
			sb.append("Creating new QuercusScriptEngine()\n");

			writer = new StringWriter();
			quercusScriptEngine = new QuercusScriptEngine();
			quercusScriptEngine.getContext().setWriter(writer);

			QuercusContext quercus = quercusScriptEngine.getQuercus();
			ModuleContext moduleContext = quercus.getModuleContext();
			moduleContext.addModule("JiraModule", jiraModule);
			moduleContext.init();

			Collection<ModuleInfo> moduleInfos = moduleContext.getModules();
			sb.append("=== getModules() found " + moduleInfos.size() + " modules: ");
			for (ModuleInfo mi : moduleInfos) {
				String name = mi.getName();
				List<String> tokens = Arrays.asList(name.split("\\."));
				name = tokens.get(tokens.size() - 1);
				sb.append(name);
				sb.append(" ");
			}
			sb.append("\n");

			log.warn(sb.toString());
			sb.setLength(0);
		} catch (Exception e) {
			log.error("Failed createQuercusScriptEngine: " + e.getMessage());
		}
	}

	private void fixQuercusClassLoader() {
		ModuleContext moduleContext = quercusScriptEngine.getQuercus().getModuleContext();
		Field loaderField;
		try {
			loaderField = ModuleContext.class.getDeclaredField("_loader");
			loaderField.setAccessible(true);
			ClassLoader moduleContextLoader = (ClassLoader) loaderField.get(moduleContext);
			ClassLoader locatorLoader = ComponentLocator.class.getClassLoader();
			if (moduleContextLoader != locatorLoader) {
				loaderField.set(moduleContext, locatorLoader);
				log.debug("fixed ModuleContext._loader, set to: " + locatorLoader.getClass().toString());
			} else {
				//log.debug("ModuleContext._loader already fixed");
			}
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	@Override
	public QuercusScriptEngine getQuercusScriptEngine() {
		writer.getBuffer().setLength(0);
		quercusScriptEngine.getQuercus().clearDefinitionCache();
		return quercusScriptEngine;
	}

	@Override
	public String getOutput() {
		String output = writer.toString();
		writer.getBuffer().setLength(0);
		return output;
	}

	@Override
	public String getWrappedScript(String src, String path) throws IOException {
		StringBuffer finalScript = new StringBuffer();

		finalScript.append("<?php ");
		finalScript.append("set_include_path('" + getScriptRoot() + "');");
		if (!Strings.isNullOrEmpty(src)) {
			finalScript.append("\n").append(src);
		}
		finalScript.append("; ?>");
		if (!Strings.isNullOrEmpty(path)) {
			String script = PHPRunnerUtils.fileToString(getScriptRoot() + "/" + path);
			finalScript.append(script);
		}
		log.debug("finalScript: " + finalScript.toString());
		return finalScript.toString();
	}

	private CompiledScript getCompiledScript(String src) throws ScriptException {

		String hash = PHPRunnerUtils.md5(src);
		if (hash == null)
			return null;

		CompiledScript script;

		synchronized (_scriptCache) {
			script = _scriptCache.get(hash);
			if (script == null) {
				final long startCompileTime = System.currentTimeMillis();
				script = quercusScriptEngine.compile(src);
				final long compileTime = System.currentTimeMillis() - startCompileTime;
				if (_scriptCache.size() == _cacheSize) {
					String removedKey = _scriptCache.removeLastAndGetKey();
					if (log.isDebugEnabled()) {
						log.debug("Removed key: " + removedKey);
					}
				}
				_scriptCache.put(hash, script);
				if (log.isDebugEnabled()) {
					log.debug("Added script to cache, hash:" + hash + ", compiled in: " + compileTime
							+ " ms, number of objects:" + _scriptCache.size());
				}
				_totalCompileTime += compileTime;
			} else {
				_scriptCache.moveToTop(hash);
				if (log.isDebugEnabled()) {
					log.debug("Script fetched from cache, hash:" + hash);
				}
			}

			if (log.isDebugEnabled()) {
				for (int i = 0; i < _scriptCache.size(); i++) {
					log.debug("_scriptCache key " + i + ": " + _scriptCache.getKeyAt(i));
				}
			}
		}

		return script;
	}

	@Override
	public void clearScriptCache() {
		if (log.isDebugEnabled()) {
			log.debug("Clearing script cache, number of objects: " + _scriptCache.size());
		}
		_scriptCache.clear();
	}

	@Override
	public Value eval(String src, String path, ScriptContext context) throws ScriptException, IOException {
		final long startTime = System.currentTimeMillis();
		Value value;
		if (_cachingEnabled) {
			value = (Value) getCompiledScript(getWrappedScript(src, path)).eval(context);
		} else {
			value = (Value) quercusScriptEngine.eval(getWrappedScript(src, path), context);
		}
		context.getErrorWriter().flush();
		context.getWriter().flush();
		final long time = System.currentTimeMillis() - startTime;
		_totalEvalTime += time;
		_evals++;
		if (log.isDebugEnabled()) {
			log.debug("eval took: " + time + " ms");
		}
		return value;
	}

	@Override
	public void install() throws Exception {
		File quercusDir = new File(PHPRunnerUtils.getScriptRoot());
		if (!quercusDir.exists()) {
			quercusDir.mkdir();
			log.warn("Created " + quercusDir.getAbsolutePath());
			FileOutputStream os = new FileOutputStream(new File(quercusDir, "php.ini"), false);
			InputStream is = this.getClass().getResource("/scriptroot/php.ini").openStream();
			IOUtils.copy(is, os);
			is.close();
			os.close();
		} else if (!quercusDir.isDirectory()) {
			log.fatal(quercusDir.getAbsolutePath() + " is not a directory!");
		}
	}

	@Override
	public long getTotalCompileTime() {
		return _totalCompileTime;
	};

	@Override
	public long getTotalEvalTime() {
		return _totalEvalTime;
	};

	@Override
	public int getScriptCacheSize() {
		return _scriptCache.size();
	}

	@Override
	public int getMaxScriptCacheSize() {
		return _cacheSize;
	}

	@Override
	public int getEvals() {
		return _evals;
	}

}
