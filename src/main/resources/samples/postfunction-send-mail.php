<?php
/*
// Expected configuration:
// $from_address = "someone@example.com"; // optional
$recipients_to = array(
    "address1@example.com", // explicit address
    "assignee",
    "reporter",
    "bill123", // userkey (not username!)
    "customfield_NNNNN", // text field validated as email
    "customfield_NNNNN", // single user picker
    "customfield_NNNNN", // multi user picker
);
// $recipients_cc = array(); // optional, like above
// $recipients_bcc = array(); // optional, like above
$template_path  = "path/to/template/file.php"; // under script root, will provide body, subject, content type
// $add_attachments = true; // optional, default is false
// $do_not_send = true; // optional, default is false;
*/

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.mail.Email;
import com.atlassian.mail.queue.SingleMailQueueItem;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeUtility;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import java.io.File;

if (!$template_path) {
    $log->error("template_path not provided");
    return false;
}
$template_path = get_include_path() . "/$template_path"; // hope only one is there
if (!is_readable($template_path)) {
    $log->error("template_path '$template_path' is not readable");
    return false;
}

$mailServerManager = ComponentAccessor::getMailServerManager();
$mailServer = $mailServerManager->getDefaultSMTPMailServer();
$mailQueue = ComponentAccessor::getMailQueue();
$customFieldManager = ComponentAccessor::getCustomFieldManager();
$attachmentManager = ComponentAccessor::getAttachmentManager();
$pathManager = ComponentAccessor::getAttachmentPathManager();
$userManager = ComponentAccessor::getUserManager();

list($body, $subject, $contenttype) = parse_template($template_path);

if (!$from_address) {
    $from_address = $mailServer->getDefaultFrom();
    $log->debug("from_address set to default ${from_address}");
}

$email = new Email(
    process_recipient_list($recipients_to),
    process_recipient_list($recipients_cc),
    process_recipient_list($recipients_bcc)
);
$email->setFrom($from_address);
$email->setSubject($subject);
$email->setMimeType($contenttype);
$email->setBody($body);

if ($add_attachments) {
    add_mail_attachments($email);
}

if (!$do_not_send) {
    $item = new SingleMailQueueItem($email);
    $mailQueue->addItem($item);    
} else {
    $log->warn("do_not_send set, email not sent");
}

//------------------------------------------------------------------

function process_recipient_list($arr) {
    global $issue, $customFieldManager, $log;
    global $userManager;

    if (!is_array($arr)) {
        $log->warn("recipient list empty");
        return null;
    }
    
    $recipients = array();
    foreach($arr as $something) {
        $log->debug("parsing recipient: '$something'");
        if ($something == "assignee") {
            $someone = $issue->getAssignee();
            if ($someone) {
                $address = $someone->getEmailAddress();
                $log->debug("assignee address '$address'");
                $recipients[] = $address;
            }
        } elseif ($something == "reporter") {
            $someone = $issue->getReporter();
            if ($someone) {
                $address = $someone->getEmailAddress();
                $log->debug("reporter address '$address'");
                $recipients[] = $address;
            }
        } elseif (preg_match("/^customfield_\d+$/",$something)) {
            $customField = $customFieldManager->getCustomFieldObject($something);
            $cf_value = $issue->getCustomFieldValue($customField);
            if (!$cf_value) {
                $log->debug("field $something is empty");
            } elseif (filter_var($cf_value, FILTER_VALIDATE_EMAIL)) {
                $log->debug("field $something explicit address $cf_value");
                $recipients[] = "$cf_value";
            } elseif (is_array($cf_value)) {
                foreach($cf_value as $someone) {
                    $someone_key = $someone->getKey();
                    $address = $someone->getEmailAddress();
                    $log->debug("customfield multi ${something} for '$someone_key' address '$address'");
                    $recipients[] = $address;
                }
            } else {
                $someone_key = $cf_value->getKey();
                $address = $cf_value->getEmailAddress();
                $log->debug("customfield single ${something} for '$someone_key' address '$address'");
                $recipients[] = $address;
            }
        } elseif (preg_match("/^(\w+)$/",$something,$match)) {
            $user_key = $match[1];
            $someone = $userManager->getUserByKey($user_key);
            if ($someone) {
                $address = $someone->getEmailAddress();
                $log->debug("userkey ${user_key} address '$address'");
                $recipients[] = $address;
            } else {
                $log->error("cannot process recipient user key '$user_key'");
            }
        } elseif (filter_var($something, FILTER_VALIDATE_EMAIL)) {
                $log->debug("explicit address '$something'");
                $recipients[] = "$something";
        } else {
            $log->error("cannot process recipient '$something'");
        }
    }
    $result = join(",",$recipients);
    $log->debug("recipients: '${result}'");
    return $result;
}


function add_mail_attachments($email) {
    global $issue, $pathManager;
    
    $attachments = $issue->getAttachments();
    if (count($attachments)>0) {
        $multiPart = new MimeMultipart("mixed");
        $attachmentsRoot = $pathManager->getAttachmentPath();
        var_dump($attachmentsRoot);
        foreach($attachments as $att) {
            $filePath = join(File::separator, array($attachmentsRoot, $issue->getProjectObject()->getKey(), $issue->getKey(), $att->getId()));
            $attFile = new File($filePath);
            $attPart = new MimeBodyPart();
            $fileDataSource = new FileDataSource($attFile);
            $attPart->setDataHandler(new DataHandler($fileDataSource));
            $fileName = $att->getFilename();
            $attPart->setFileName(MimeUtility::encodeText($fileName));
            $log->debug("Attaching ${fileName} to mail");
            $multiPart->addBodyPart($attPart);
        }
        $email->setMultipart($multiPart);
    }
}

function parse_template($template_path) {
    global $issue;
    global $customFieldManager;
    global $log;
    
    $summary = $issue->getSummary();
    $description = $issue->getDescription();
    $issuekey = $issue->getKey();
    $assignee = $issue->getAssignee();

    foreach ($customFieldManager->getCustomFieldObjects($issue) as $customfield) {
        $cfname = $customfield->getId();
        $value = $issue->getCustomFieldValue($customfield);
        $log->debug("field ${cfname}:${value}");
        $$cfname = $value; // Variable variables
    }

    ob_start();
    list($subject,$contenttype)=require($template_path);
    $body = ob_get_contents();
    ob_end_clean();

    $log->debug("subject:${subject}");
    $log->debug("body:${body}");
    return array($body,$subject,$contenttype);
}

?>