package ua.com.able.jira.plugins.phprunner.action;

import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.type.filter.AssignableTypeFilter;

//import com.atlassian.jira.event.AbstractProjectEvent;
//import com.atlassian.jira.event.AbstractWorkflowEvent;
import com.atlassian.jira.event.JiraEvent;
import com.atlassian.jira.event.bc.project.component.AbstractProjectComponentEvent;
//import com.atlassian.jira.event.issue.field.AbstractCustomFieldEvent;
import com.atlassian.jira.event.project.AbstractVersionEvent;
import com.atlassian.jira.event.user.UserEvent;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.crowd.event.DirectoryEvent;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.event.type.EventTypeManager;
import com.atlassian.jira.util.ComponentLocator;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import com.atlassian.webresource.api.assembler.PageBuilderService;

import ua.com.able.jira.plugins.phprunner.listen.ao.ListenerEntity;
import ua.com.able.jira.plugins.phprunner.listen.ao.ListenerEntityManager;
import ua.com.able.jira.plugins.phprunner.utils.PHPRunnerUtils;

@WebSudoRequired
public class PHPRunnerListeners extends JiraWebActionSupport {

	private static final long serialVersionUID = -8437800470328741714L;
	private static final Logger log = LoggerFactory.getLogger(PHPRunnerListeners.class);
    @SuppressWarnings("unused")
	private final I18nHelper i18nHelper;
    private final PageBuilderService pageBuilderService;
    private final ListenerEntityManager listenerEntityManager;
    private Map<String,Object> config;
    ListenerEntity listenerEntity;

    public PHPRunnerListeners(final PageBuilderService pageBuilderService,
    		final ListenerEntityManager listenerEntityManager,
    		final I18nHelper i18nHelper)
    {
    	super();
        this.pageBuilderService = pageBuilderService;
        this.listenerEntityManager = listenerEntityManager;
        this.i18nHelper = i18nHelper;
        // addErrorMessage(String.format(i18nHelper.getText("groupeditor.error.user.empty")));
        this.config = new HashMap<String,Object>();
    }

    @Override
	protected String doExecute() throws Exception {
        //log.warn("Entering doExecute");
        this.pageBuilderService.assembler().resources()
        	.requireWebResource("ua.com.able.jira.plugins.phprunner:phprunner-resources");
        return SUCCESS;
    }

    public String doDelete() throws Exception {
        String idStr = getHttpRequest().getParameter("id");
        int id = Integer.parseInt(idStr);
        if (id != 0) {
            listenerEntityManager.deleteEntityById(id);
        }
    	return getRedirectToList();
    }

	public String doAdd() throws Exception {
        this.pageBuilderService.assembler().resources()
	    	.requireWebResource("ua.com.able.jira.plugins.phprunner:phprunner-resources")
	    	.requireWebResource("ua.com.able.jira.plugins.phprunner:codemirror");
        config.put("action", "add");
        listenerEntity = listenerEntityManager.getNewEntity();

        if (! "save".equals(getHttpRequest().getParameter("action"))) {
            return INPUT;
        }

        if (!getValidEntityFromRequest(listenerEntity)) {
            return INPUT;
        }
        listenerEntityManager.createAndSave(listenerEntity);
        log.warn("listenerEntity saved");
        return getRedirectToList();
    }


	public String doEdit() throws Exception {
        this.pageBuilderService.assembler().resources()
	    	.requireWebResource("ua.com.able.jira.plugins.phprunner:phprunner-resources")
	    	.requireWebResource("ua.com.able.jira.plugins.phprunner:codemirror");
        config.put("action", "edit");

        String idStr = getHttpRequest().getParameter("id");
        int id = Integer.parseInt(idStr);
        if (id == 0)
        	return getRedirectToList();

        listenerEntity = listenerEntityManager.getEntityById(id);

        if (listenerEntity == null)
        	return getRedirectToList();

        if (! "save".equals(getHttpRequest().getParameter("action"))) {
            return INPUT;
        }

        if (!getValidEntityFromRequest(listenerEntity)) {
            return INPUT;
        }

        listenerEntity.save();
        return getRedirectToList();
    }


	private String getRedirectToList() {
		return getRedirect(this.getClass().getSimpleName()+".jspa");
	}


	private boolean getValidEntityFromRequest(ListenerEntity entity) {
		HttpServletRequest req = getHttpRequest();
		boolean valid = true;
		String inlineSrc = PHPRunnerUtils.emptyNotNull(req.getParameter("inlineSrc"));
		String fileSrc = PHPRunnerUtils.emptyNotNull(req.getParameter("fileSrc")).trim();
		String[] projects = req.getParameterValues("projects");
		String[] events = req.getParameterValues("events");
		if (inlineSrc.trim() == "" && fileSrc == "") {
			valid = false;
		}
		if (projects == null || projects.length==0) {
			valid = false;
		} else {
			entity.setProjects(ListenerEntityManager.convertProjectsToString(projects));
		}
		if (events == null || events.length==0) {
			valid = false;
		} else {
			entity.setEvents(ListenerEntityManager.convertEventTypesToString(events));
		}
		entity.setDescription(PHPRunnerUtils.emptyNotNull(req.getParameter("description")));
		entity.setInlineScript(inlineSrc);
		entity.setFilePath(fileSrc);
		return valid;
	}


	// thanks, Jamie
	public Map<String,Map<String,String>> getEventsByType () {

		Map<String,Map<String,String>> allEvents = new LinkedHashMap<String, Map<String, String>>();

		EventTypeManager eventTypeManager = ComponentAccessor.getEventTypeManager();
		Map<String,String> issueEvents = new LinkedHashMap<String, String>();
		issueEvents.put("", "All Issue Events");
		for (EventType et: eventTypeManager.getEventTypes()) {
			issueEvents.put(et.getId().toString(), et.getName());
		}
		allEvents.put("Issue Events", issueEvents);

		ClassLoader webAppClassLoader = ComponentLocator.class.getClassLoader();
		ClassPathScanningCandidateComponentProvider provider = new ClassPathScanningCandidateComponentProvider(true);

		provider.addIncludeFilter(new AssignableTypeFilter(JiraEvent.class));
        provider.addIncludeFilter(new AssignableTypeFilter(AbstractVersionEvent.class));
        provider.addIncludeFilter(new AssignableTypeFilter(AbstractProjectComponentEvent.class));
        provider.addIncludeFilter(new AssignableTypeFilter(DirectoryEvent.class));

        // provider.addIncludeFilter(new AssignableTypeFilter(AbstractProjectEvent.class));
        // provider.addIncludeFilter(new AssignableTypeFilter(AbstractWorkflowEvent.class));
        // provider.addIncludeFilter(new AssignableTypeFilter(AbstractCustomFieldEvent.class));

        provider.setResourceLoader(new PathMatchingResourcePatternResolver(webAppClassLoader));
        Set<BeanDefinition> candidateComponents = provider.findCandidateComponents("com.atlassian.jira.event");
        candidateComponents.addAll(provider.findCandidateComponents("com.atlassian.crowd.event"));

        for(BeanDefinition bd: candidateComponents) {
        	String eventClassName = bd.getBeanClassName();
        	try {
				Class<?> clazz = webAppClassLoader.loadClass(eventClassName);

				if (Modifier.isAbstract(clazz.getModifiers())) {
					log.debug("ignoring eventClassName: " + eventClassName + " - is abstract");
				} else if (
						AbstractVersionEvent.class.isAssignableFrom(clazz) ||
						AbstractProjectComponentEvent.class.isAssignableFrom(clazz) ||
						UserEvent.class.isAssignableFrom(clazz) ||
						DirectoryEvent.class.isAssignableFrom(clazz) ||

						// AbstractProjectEvent.class.isAssignableFrom(clazz) ||
						// AbstractWorkflowEvent.class.isAssignableFrom(clazz) ||
						// AbstractCustomFieldEvent.class.isAssignableFrom(clazz) ||

						false
		        	)
				{
					String category = "Others";
					if (DirectoryEvent.class.isAssignableFrom(clazz)) category = "Directory Events";
					if (AbstractVersionEvent.class.isAssignableFrom(clazz)) category = "Version Events";
					if (AbstractProjectComponentEvent.class.isAssignableFrom(clazz)) category = "Project Component Events";
					if (UserEvent.class.isAssignableFrom(clazz)) category = "User Event";

					// if (AbstractProjectEvent.class.isAssignableFrom(clazz)) category = "Project Events";
					// if (AbstractWorkflowEvent.class.isAssignableFrom(clazz)) category = "Workflow Events";
					// if (AbstractCustomFieldEvent.class.isAssignableFrom(clazz)) category = "Custom Field Events";

					if (!allEvents.containsKey(category))
						allEvents.put(category, new LinkedHashMap<String, String>());

					allEvents.get(category).put(clazz.getName(), clazz.getSimpleName());
					log.debug("eventClassName:" + eventClassName + ", category:" + category);
				} else {
					log.debug("ignoring eventClassName:" + eventClassName);
				}

			} catch (ClassNotFoundException e) {
				log.error("Cannot load eventClassName:" + eventClassName);
			}
        }

        sortEvents(allEvents);

		return allEvents;
	}

	@SuppressWarnings("unchecked")
	private static void sortEvents(Map<String,Map<String, String>> allEvents) {
        for(String key: allEvents.keySet()) {
        	if (key != "Issue Events") {
        		Map<String, String> map = allEvents.get(key);
        		allEvents.put(key, (Map<String, String>) PHPRunnerUtils.sortByValue(map));
        	}
        }
	}

	public List<Project> getProjects() {
		return ComponentAccessor.getProjectManager().getProjectObjects();
	}

	public Map<String,Object> getConfig() {
		return config;
	}

	public ListenerEntity[] getListenerEntities() {
		return listenerEntityManager.getEntities();
	}

	public ListenerEntity getListenerEntity() {
		return listenerEntity;
	}

	public boolean isProjectSelected(Long id) {
		if (listenerEntity.getProjects() == null)
			return false;
		List<String> projectIds = Arrays.asList(listenerEntity.getProjects().split(","));
		//log.warn("projectIds:" + projectIds.toString() + " id:" + id.toString());
		return projectIds.contains(id.toString());
	}

	public boolean isAllProjectsSelected() {
		if (listenerEntity.getProjects() == null)
			return false;
		List<String> projectIds = Arrays.asList(listenerEntity.getProjects().split(","));
		//log.warn("projectIds:" + projectIds.toString());
		return projectIds.contains("");
	}

	public boolean isEventSelected(String id) {
		if (listenerEntity.getEvents() == null)
			return false;
		List<String> eventIds = Arrays.asList(listenerEntity.getEvents().split(","));
		//log.warn("eventIds:" + eventIds.toString() + " id:" + id);
		return eventIds.contains(id);
	}


	public String getProjectListHtml(ListenerEntity entity) {
		String pids = entity.getProjects();
		if (pids == null)
			return "";
		StringBuffer sb = new StringBuffer();
		List<String> projectIds = Arrays.asList(pids.split(","));
		ProjectManager pm = ComponentAccessor.getProjectManager();
		for(String pid: projectIds) {
			if ("".equals(pid)) {
				sb.append("All Projects");
				sb.append("<br>");
			} else {
				Project proj = pm.getProjectObj(Long.parseLong(pid));
				if (proj != null) {
					sb.append(proj.getName());
					sb.append(" (");
					sb.append(proj.getKey());
					sb.append(")<br>");
				}
			}
		}
		return sb.toString();
	}

	public String getEventListHtml(ListenerEntity entity) {
		String eids = entity.getEvents();
		if (eids == null)
			return "";
		StringBuffer sb = new StringBuffer();
		List<String> eventIds = Arrays.asList(eids.split(","));
		EventTypeManager etm = ComponentAccessor.getEventTypeManager();
		for(String eid: eventIds) {
			if ("".equals(eid)) {
				sb.append("All Issue Events");
				sb.append("<br>");
			} else {
				try {
					EventType type = etm.getEventType(Long.parseLong(eid));
					sb.append(type.getName());
				} catch (NumberFormatException e) {
					String[] splitClass = eid.split("\\.");
					//log.warn("splitClass:"+splitClass.length);
					sb.append(splitClass[splitClass.length-1]);
				}
				sb.append("<br>");
			}
		}
		return sb.toString();
	}

}
