package ua.com.able.jira.plugins.phprunner.action;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.webresource.api.assembler.PageBuilderService;

import ua.com.able.jira.plugins.phprunner.utils.PHPRunnerUtils;
import ua.com.able.jira.plugins.phprunner.webserv.ao.WebServerContextEntity;
import ua.com.able.jira.plugins.phprunner.webserv.ao.WebServerContextEntityManager;

public class PHPRunnerWebServerContexts extends JiraWebActionSupport {

	private static final long serialVersionUID = 6516970990671434239L;
	private static final Logger log = LoggerFactory.getLogger(PHPRunnerWebServerContexts.class);
    private final PageBuilderService pageBuilderService;
	private final WebServerContextEntityManager webServerContextEntityManager;
	private Map<String,Object> config;
	private Map<String,Object> entityParams;
	private String jiraBaseUrl;
	//private WebServerContextEntity entity;

	public PHPRunnerWebServerContexts(final PageBuilderService pageBuilderService,
			final WebServerContextEntityManager webServerContextEntityManager)
	{
		super();
        this.pageBuilderService = pageBuilderService;
		this.webServerContextEntityManager = webServerContextEntityManager;
        this.config = new HashMap<String,Object>();
	}

	@Override
	protected String doExecute() throws Exception {
        //log.warn("Entering doExecute");
        this.pageBuilderService.assembler().resources()
        	.requireWebResource("ua.com.able.jira.plugins.phprunner:phprunner-resources");
        this.jiraBaseUrl = ComponentAccessor.getApplicationProperties().getString(APKeys.JIRA_BASEURL);
        return SUCCESS;
    }

	public String doAdd() throws Exception {
        this.pageBuilderService.assembler().resources()
	    	.requireWebResource("ua.com.able.jira.plugins.phprunner:phprunner-resources");
        config.put("action", "add");
        if (! "save".equals(getHttpRequest().getParameter("action"))) {
            return INPUT;
        }
        if (!getValidEntityFromRequest()) {
            return INPUT;
        }
        webServerContextEntityManager.create(entityParams);
        log.warn("entity saved");
        return getRedirectToList();
    }

	public String doEdit() throws Exception {
        this.pageBuilderService.assembler().resources()
	    	.requireWebResource("ua.com.able.jira.plugins.phprunner:phprunner-resources");
        config.put("action", "edit");
        if (! "save".equals(getHttpRequest().getParameter("action"))) {
    		String idStr = PHPRunnerUtils.emptyNotNull(getHttpRequest().getParameter("id"));
    		Integer id = (idStr != "") ? Integer.valueOf(idStr) : null;
    		WebServerContextEntity e = webServerContextEntityManager.getById(id);
    		if (e == null) {
    			addErrorMessage("hacking id?");
    		} else {
    	        entityParams = new HashMap<String,Object>();
    			entityParams.put("id",id);
    			entityParams.put("context",e.getContext());
    			entityParams.put("documentRoot",e.getDocumentRoot());
    			entityParams.put("requiresAuthenticatedUser", e.isRequiresAuthenticatedUser() ? "true" : "");
    			entityParams.put("requiresWebSudo", e.isRequiresWebSudo() ? "true" : "");
    		}

            return INPUT;
        }
        if (!getValidEntityFromRequest()) {
            return INPUT;
        }
        webServerContextEntityManager.update(entityParams);
        log.warn("entity updated");
        return getRedirectToList();
    }

	public String doDelete() throws Exception {
        this.pageBuilderService.assembler().resources()
	    	.requireWebResource("ua.com.able.jira.plugins.phprunner:phprunner-resources");
		String idStr = PHPRunnerUtils.emptyNotNull(getHttpRequest().getParameter("id"));
		Integer id = (idStr != "") ? Integer.valueOf(idStr) : null;
		WebServerContextEntity e = webServerContextEntityManager.getById(id);
		if (e != null) {
	        webServerContextEntityManager.delete(id.intValue());
	        log.warn("entity deleted, id: " + id.toString());
		}
        return getRedirectToList();
    }

	private boolean getValidEntityFromRequest() {
        entityParams = new HashMap<String,Object>();
		HttpServletRequest req = getHttpRequest();
		String idStr = PHPRunnerUtils.emptyNotNull(req.getParameter("id"));
		Integer id = (idStr != "") ? Integer.valueOf(idStr) : null;
		String action = (String) config.get("action");
		String context = PHPRunnerUtils.emptyNotNull(req.getParameter("context")).trim();
		String documentRoot = PHPRunnerUtils.emptyNotNull(req.getParameter("documentRoot")).trim();
		String auth = PHPRunnerUtils.emptyNotNull(req.getParameter("requiresAuthenticatedUser"));
		String sudo = PHPRunnerUtils.emptyNotNull(req.getParameter("requiresWebSudo"));
		entityParams.put("id",id);
		entityParams.put("context",context);
		entityParams.put("documentRoot",documentRoot);
		entityParams.put("requiresAuthenticatedUser", auth);
		entityParams.put("requiresWebSudo", sudo);
		//this.getFlushedErrorMessages();
		if ("".equals(context)) {
			addErrorMessage("Empty context");
		} else {
			WebServerContextEntity e = webServerContextEntityManager.getByContext(context);
			if (e != null) {
				if (action == "add" || action == "edit" && e.getID() != id.intValue() ) {
					addErrorMessage("Entity with the same context already exists");
				}
			}
		}
		if ("".equals(documentRoot)) {
			addErrorMessage("Must provide directory path");
		} else {
			File dir = new File(documentRoot);
			if (!dir.isDirectory()) {
				addErrorMessage("Provided path is not directory");
			}
		}
		if (id != null) {
			WebServerContextEntity e = webServerContextEntityManager.getById(id);
			if (e == null) {
				addErrorMessage("Entity does not exist, ID: " + id.toString());
			}
		}
		log.debug("errors: " + getErrorMessages().size());
		return !getHasErrorMessages();
	}

	private String getRedirectToList() {
		return getRedirect(this.getClass().getSimpleName()+".jspa");
	}

	public Map<String,Object> getConfig() {
		return config;
	}

	public Map<String,Object> getEntityParams() {
		return entityParams;
	}

	public WebServerContextEntity[] getAllEntities() {
		return webServerContextEntityManager.getAll();
	}

	public String getContextLink(String context) {
		return jiraBaseUrl + "/plugins/servlet/phpwebserv/" + context + "/";
	}
}
