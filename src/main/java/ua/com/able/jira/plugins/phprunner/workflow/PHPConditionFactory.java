package ua.com.able.jira.plugins.phprunner.workflow;

import com.atlassian.jira.plugin.workflow.WorkflowPluginConditionFactory;
import com.atlassian.webresource.api.assembler.PageBuilderService;

public class PHPConditionFactory extends AbstractPHPWorkflowPluginFactory implements WorkflowPluginConditionFactory  {

	public PHPConditionFactory(final PageBuilderService pageBuilderService) {
		super(pageBuilderService);
	}

}
