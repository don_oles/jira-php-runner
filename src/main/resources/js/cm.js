(function ($) {

	console.log("entered cm.js");
	
    var reinitialiseAll = function () {
    	console.log("entered reinitialiseAll");
        convertToCm();
        //codeCheckFiles();
    };
	
    var convertToCm = function () {
    	console.log("entered convertToCm");
        $("textarea.CodeMirror").each(function () {
            var $textarea = $(this);

            // If this component is already a code mirror text area we just skip it
            if ($textarea.data('CodeMirrorInstance')){
                return;
            }

            var language = $textarea.data("sr-lang");

        	console.log("found textarea with " + language);
            
            var codeMirrorOptions = {
                lineNumbers: true,
                matchBrackets: true,
                indentUnit: 4,
                enterMode: "keep",
                width: "400px",
                electricChars: false,
                theme: "blackboard",
                extraKeys: {
                    "F11": function(cm) {
                        cm.setOption("fullScreen", !cm.getOption("fullScreen"));
                    },
                    "Esc": function(cm) {
                        if (cm.getOption("fullScreen")) cm.setOption("fullScreen", false);
                    }
                },
                styleActiveLine: true,
                gutters: ["CodeMirror-lint-markers"],
                autoCloseBrackets: true
            };

            // set default options for supported languages
            switch (language) {
                case "php":
                    codeMirrorOptions.mode = "text/x-php";
                    break;
                default:
                    codeMirrorOptions.mode = "text/plain";
            }

            console.log("mode set to " + codeMirrorOptions.mode);

            if ($textarea.hasClass("readonly")) {
                codeMirrorOptions["nocursor"] = true; // this work in CM2/3
                codeMirrorOptions["readOnly"] = true;
            }

            var cm = CodeMirror.fromTextArea($textarea[0], codeMirrorOptions);
            $textarea.data('CodeMirrorInstance', cm);

            if ($textarea.hasClass("readonly")) {
                $textarea.next("div.CodeMirror").css("background-color", "#383838");
            }

            cm.on("change", function (cmdoc) {
                $textarea.val(cmdoc.getValue());
            });

            cm.refresh();
        });
    };
    
    PHPRunner.Base.convertToCm = convertToCm;
    
    AJS.toInit(reinitialiseAll);

})(AJS.$);
