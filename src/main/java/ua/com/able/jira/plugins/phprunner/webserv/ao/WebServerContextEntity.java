package ua.com.able.jira.plugins.phprunner.webserv.ao;

import net.java.ao.Entity;
import net.java.ao.Preload;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.Table;
import net.java.ao.schema.Unique;

@Preload
@Table("WSCONTEXT")
public interface WebServerContextEntity extends Entity {

    @NotNull
    @Unique
	String getContext();
	void setContext(String context);

    @NotNull
	String getDocumentRoot();
	void setDocumentRoot(String documentRoot);

	boolean isRequiresAuthenticatedUser();
	void setRequiresAuthenticatedUser(boolean requiresAuthenticatedUser);

	boolean isRequiresWebSudo();
	void setRequiresWebSudo(boolean requiresWebSudo);
}
