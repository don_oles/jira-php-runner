package ua.com.able.jira.plugins.phprunner.listen;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;

import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.SimpleBindings;
import com.caucho.quercus.env.Value;
import com.caucho.quercus.script.QuercusScriptEngine;

import org.apache.log4j.Logger;

import com.atlassian.jira.event.project.AbstractVersionEvent;
import com.atlassian.jira.event.ClearCacheEvent;
import com.atlassian.jira.event.bc.project.component.AbstractProjectComponentEvent;
import com.atlassian.crowd.event.DirectoryEvent;
import com.atlassian.jira.event.user.UserEvent;
import com.atlassian.event.api.EventListener;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.project.Project;

import ua.com.able.jira.plugins.phprunner.listen.ao.ListenerEntity;
import ua.com.able.jira.plugins.phprunner.listen.ao.ListenerEntityManager;
import ua.com.able.jira.plugins.phprunner.quercus.ScriptingManager;


public class PHPRunnerSystemListener  {

	private ListenerEntityManager listenerEntityManager;
	private final ScriptingManager scriptingManager;

	private static final Logger log = Logger.getLogger(PHPRunnerSystemListener.class);

	public PHPRunnerSystemListener(ListenerEntityManager listenerEntityManager, final ScriptingManager scriptingManager) {
		this.listenerEntityManager = listenerEntityManager;
		this.scriptingManager = scriptingManager;
		log.debug("PHPRunnerListener()");
	}

    private void runEvent(final Object event) {

    	log.debug("runEvent: " + event.toString());
    	String eventTypeId = event.getClass().getName();
    	String projectId = null;
    	Project project = null;
        MutableIssue issue = null;
        if (event instanceof IssueEvent) {
        	IssueEvent issueEvent = (IssueEvent) event;
        	issue = (MutableIssue) issueEvent.getIssue();
            eventTypeId = issueEvent.getEventTypeId().toString();
            project = issueEvent.getProject();
            log.debug("IssueEvent: "+issue.getKey()+"/"+eventTypeId);
        } else {
        }

        if (project != null) {
            projectId = project.getId().toString();
        }

        List<ListenerEntity> entities = listenerEntityManager.getRelevantListeners(projectId,eventTypeId);
        for(ListenerEntity e: entities) {
        	log.debug("Found listener: '" + e.getDescription() + "' [" + e.getProjects() + "] [" + e.getEvents() + "]");

//        	PrintWriter stdoutWriter = new PrintWriter(System.out, true);
        	PrintWriter stderrWriter = new PrintWriter(System.err, true);
    		StringWriter stdoutWriter = new StringWriter();
//    		StringWriter stderrWriter = new StringWriter();

    	    @SuppressWarnings("unused")
    		Value value = null;

    		try {

    			QuercusScriptEngine quercusScriptEngine = scriptingManager.getQuercusScriptEngine();
    		    Bindings bindings = new SimpleBindings();
    		    bindings.put("event", event);
    	        if (event instanceof IssueEvent) {
        		    bindings.put("project", project);
        		    bindings.put("issue", issue);
    	        }
    		    bindings.put("log", log);
    		    quercusScriptEngine.getContext().setWriter(stdoutWriter);
    		    quercusScriptEngine.getContext().setErrorWriter(stderrWriter);
    		    quercusScriptEngine.getContext().setBindings(bindings, ScriptContext.ENGINE_SCOPE);

    		    value = scriptingManager.eval(e.getInlineScript(), e.getFilePath(),quercusScriptEngine.getContext());

    		} catch (Exception ex) {
    			log.error("Exception while executing PHP script: " + ex.getMessage(), ex);
    			stderrWriter.append(ex.getMessage());
    		}

    		stdoutWriter.flush();
    		stderrWriter.flush();

    		if (log.isDebugEnabled()) {
    			log.debug("stdout: "  + stdoutWriter.toString());
    		}

        }
    }

    @EventListener
    public void onAnyEvent(final Object event) {

    	if (
    			event instanceof IssueEvent ||
    			event instanceof DirectoryEvent ||
    			event instanceof UserEvent ||
    			event instanceof AbstractVersionEvent ||
    			event instanceof AbstractProjectComponentEvent
    	) {
            runEvent(event);
    	}
    }

    @EventListener
    public void onClearCacheEvent(final ClearCacheEvent event) {
    	scriptingManager.clearScriptCache();
    }

}


