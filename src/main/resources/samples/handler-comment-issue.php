<?php

$log->warn("Hello from mail handler!");

$subject = $message->getSubject();

if (!$handler->canHandleMessage($message)) {
    return false;
}

$log->debug("Looking for Issue Key in subject '$subject'.");
$issue = $handler->findIssueObjectInString($subject);
if ($issue == null) {
    $issue = $handler->getAssociatedIssue($message);
}
if ($issue == null) {
    return false;
}

$body = ($params["stripquotes"] == "yes") ? $handler->stripQuotedLines($handler->getEmailBody($message)) : $handler->getEmailBody($message);

$reporter = $handler->getReporter($message);

if ($context->isRealRun() && ! $handler->canAddComments($reporter, $issue)) {
    $errtext = "User $reporter does not have permission to add comments to $issue";
    $log->warn($errtext);
    $context->getMonitor()->markMessageForDeletion($errtext);
    return true;
}

$attachments = $handler->prepareAttachmentsForMessage($message, $issueObject);
foreach($attachments as $att) {
    $log->debug("I see attachment ${att->md5} ${att->fileName}");
    if ($att->md5 == "750DBA108138D707044355813A3D2483") {
        $att->skip("Just because it sucks");
    }
}

if ($context->isRealRun()) {
    $comment = $context->createComment($issue, $reporter, $body, false);    
    $attachmentsChangeItems = $handler->createPreparedAttachments($reporter, $issue);
    $handler->updateHistoryAndFireEvent($attachmentsChangeItems, $issue, $reporter, $comment, false); // false = do not force Issue Commented event
}

return true;

?>