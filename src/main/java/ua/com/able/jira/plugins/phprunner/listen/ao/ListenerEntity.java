package ua.com.able.jira.plugins.phprunner.listen.ao;

import net.java.ao.Entity;
import net.java.ao.Preload;
import net.java.ao.schema.StringLength;
import net.java.ao.schema.Table;

@Preload
@Table("LISTENERS")
public interface ListenerEntity extends Entity {

	@StringLength(StringLength.UNLIMITED)
	String getInlineScript();
	@StringLength(StringLength.UNLIMITED)
	String getProjects();
	@StringLength(StringLength.UNLIMITED)
	String getEvents();

	String getFilePath();
	String getDescription();

	void setInlineScript(String inlineScript);
	void setFilePath(String filePath);
	void setProjects(String projects);
	void setEvents(String events);
	void setDescription(String description);
}
