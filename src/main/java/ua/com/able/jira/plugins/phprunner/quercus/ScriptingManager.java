package ua.com.able.jira.plugins.phprunner.quercus;

import java.io.IOException;

import javax.script.ScriptContext;
import javax.script.ScriptException;

import com.caucho.quercus.env.Value;
import com.caucho.quercus.script.QuercusScriptEngine;

public interface ScriptingManager {

	public String getScriptRoot();

	public QuercusScriptEngine getQuercusScriptEngine();

	public String getOutput();

	public String getWrappedScript(String src, String path) throws IOException;

	public Value eval(String src, String path, ScriptContext context) throws ScriptException, IOException;

	public void create();

	public void init();

	public void install() throws Exception;

	void clearScriptCache();

	int getScriptCacheSize();

	long getTotalCompileTime();

	long getTotalEvalTime();

	int getMaxScriptCacheSize();

	int getEvals();

}
