var PHPRunner = PHPRunner || {};

PHPRunner.Base = (function ($) {
	
	var runner = {};
    
	function isWorkflowFunctions() {
		//console.log('isWorkflowFunctions()');
        return location.href.indexOf("/admin/workflows") > 0;
    }

	function isConsole() {
		//console.log('isConsole()');
        return location.href.indexOf("/admin/PHPRunnerConsole") > 0;
    }

	function isMailHandlerConfig() {
		//console.log('isMailHandlerConfig()');
        return (location.href.indexOf("/admin/EditServerDetails") > 0) || (location.href.indexOf("/admin/IncomingMailServers") > 0);
    }

	function populateFileSelectors() {
		console.log('populateFileSelectors()');
    	if ($(".phprunner-file-selector").length) {
    		$.ajax({
                type: "GET",
                url: contextPath + "/rest/phprunner/1.0/scripts",
                success: function(data){
            		$(".phprunner-file-selector").autocomplete({source: data.scripts});
                },
    		});
    	}
    }
	
	function checkMailHandlerFormSubmit(e) {
		console.log('checkMailHandlerFormSubmit()');
    	var passed = true;
    	if ($("#php-console-script").val().trim() == "") {
    		passed = false;
    		$("div#inline-error").text("Please enter some code or specify script").show();
    	} else {
    		$("div#inline-error").text("").hide();
    	}
    	if ($("#php-console-file").val().trim() == "") {
    		passed = false;
    		$("div#file-error").text("Please enter a path to a script").show();
    	} else {
    		$("div#file-error").text("").hide();
    	}
    	return passed;
    }
	
	function hookUpMailHandlerForm() {
        $("form#mailHandlerForm").submit(checkMailHandlerFormSubmit);
	}

	runner.isWorkflowFunctions = isWorkflowFunctions;
	runner.isConsole = isConsole;
	runner.populateFileSelectors = populateFileSelectors;
	runner.isMailHandlerConfig = isMailHandlerConfig;
	runner.checkMailHandlerFormSubmit = checkMailHandlerFormSubmit;
	runner.hookUpMailHandlerForm = hookUpMailHandlerForm;
	
	console.log('return runner');
	return runner;
	
}(AJS.$));


(function ($) {
    $(function() {
    	
    	
    	PHPRunner.Base.populateFileSelectors();

    	// listener projects and events parameters
    	//console.log('transforming auiSelect2');
        $("select.aui-select2").auiSelect2();
        
        $("form#php-listner-delete-form").submit(function(e){
        	var passed = $("input#listener-delete-confirm").is(':checked');
        	if (passed) {
        		$("div#delete-confirm-error").hide()
        	} else {
        		$("div#delete-confirm-error").show()
        	}
        	return passed;
        });

        
        $("form#php-listner-form").submit(function(e){
        	//console.log("submit!");
        	var passed = true;
        	var projects = $("select#php-listener-projects").val();
        	if (projects == null) {
        		passed = false;
        		$("div#projects-error").text("Please select the projects filter or All Projects").show();
        	} else if (projects.length>1 && projects[0]=="") {
        		passed = false;
        		$("div#projects-error").text("Either deselect All Projects or all other projects").show();
        	} else {
        		$("div#projects-error").text("").hide();
        	}
        	if ($("select#php-listener-events").val() == null) {
        		passed = false;
        		$("div#events-error").text("Please select events to catch").show();
        	} else {
        		$("div#events-error").text("").hide();
        	}
        	if ($("#php-console-script").val().trim() == "" && $("#php-console-file").val().trim() == "") {
        		passed = false;
        		$("div#inline-error").text("Please enter some code or specify script").show();
        	} else {
        		$("div#inline-error").text("").hide();
        	}
        	return passed;
        });
    	

        if (PHPRunner.Base.isWorkflowFunctions()) {
        	PHPRunner.Base = (function (my, $) {
            	console.log('function (my, $)');
            	$(function() {
                    // make things look a bit nicer css-wise
                	console.log('make things look a bit nicer css-wise');
                    var $form = $("#php-scripts").closest("form");
                    $form.addClass("aui");
                    $("#php-scripts").closest("table.jiraform").removeClass("jiraform");
                });
            	return my;
            }(PHPRunner.Base, AJS.$));
        }
        
        if (PHPRunner.Base.isConsole()) {
    		console.log("Hello Console");

    		$("#php-console-form").submit(function (e) {
    		      e.preventDefault();
    		});
    		
    		$("#php-console-run").click( function() {
    			console.log("Clicked!");
        		$("#php-console-stdout").empty();
        		$("#php-console-stderr").empty();
        		var data = {inlineScript: $("#php-console-script").val() , filePath: $("#php-console-file").val() };
    			$.ajax({
                    //type: "GET",
                    //url: contextPath + "/rest/phprunner/1.0/test",
                    type: "POST",
                    url: contextPath + "/rest/phprunner/1.0/run",
                    //url: contextPath + "/plugins/servlet/PHPRunnerRestServlet",
                    data: JSON.stringify(data),
                    contentType: "application/json;charset=UTF-8",
                    dataType: "json",
                    success: function(data){
                		$("#php-console-stdout").append(data.stdout);
                		$("#php-console-stderr").append(data.stderr);
                    },
    		        error: function(xhr, ajaxOptions, thrownError) {
    		            alert(xhr.status);
    		            alert(thrownError);
    		            $("#php-console-stderr").append(thrownError);
    		        }
                });
    		});
        }
        
    })
}) (AJS.$);
