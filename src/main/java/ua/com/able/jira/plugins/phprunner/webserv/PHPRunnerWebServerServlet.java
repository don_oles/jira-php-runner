package ua.com.able.jira.plugins.phprunner.webserv;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.util.JiraHome;
import com.atlassian.jira.user.ApplicationUser;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.sal.api.websudo.WebSudoManager;
import com.caucho.quercus.QuercusContext;
import com.caucho.quercus.env.Env;
import com.caucho.quercus.page.QuercusPage;
import com.caucho.quercus.script.QuercusScriptEngine;
import com.caucho.quercus.servlet.api.QuercusHttpServletRequest;
import com.caucho.quercus.servlet.api.QuercusHttpServletRequestImpl;
import com.caucho.quercus.servlet.api.QuercusHttpServletResponse;
import com.caucho.quercus.servlet.api.QuercusHttpServletResponseImpl;
import com.caucho.vfs.Path;
import com.caucho.vfs.Vfs;
import com.caucho.vfs.WriteStream;
import com.google.common.collect.ImmutableMap;

import ua.com.able.jira.plugins.phprunner.quercus.ScriptingManager;
import ua.com.able.jira.plugins.phprunner.webserv.ao.WebServerContextEntity;
import ua.com.able.jira.plugins.phprunner.webserv.ao.WebServerContextEntityManager;

public class PHPRunnerWebServerServlet extends HttpServlet {

	private static final long serialVersionUID = 6027389698069380662L;
	private static final Logger log = LoggerFactory.getLogger(PHPRunnerWebServerServlet.class);
    private final WebSudoManager webSudoManager;
    private final ScriptingManager scriptingManager;
	private final WebServerContextEntityManager webServerContextEntityManager;
	private final static String jiraHome = ComponentAccessor.getComponentOfType(JiraHome.class).getHome().toString();

	private final static Map<String,String> extentionsMap = ImmutableMap.<String, String>builder()
			.put("js","application/javascript")
			.put("css","text/css")
			.put("jpg","image/jpeg")
			.put("jpeg","image/jpeg")
			.put("gif","image/gif")
			.put("png","image/png")
			.put("txt","text/plain")
			.put("html","text/html")
			.put("htm","text/html")
			.put("php","php")
			.build();

    private String webServerContext;
    private String webServerPathInfo;
    private String webServerContextRootDirectory;
    private String contentType;
    private File file;

	public PHPRunnerWebServerServlet (final WebSudoManager webSudoManager,
			final WebServerContextEntityManager webServerContextEntityManager,
			final ScriptingManager scriptingManager )
	{
		super();
		this.scriptingManager = scriptingManager;
		this.webServerContextEntityManager = webServerContextEntityManager;
		this.webSudoManager = webSudoManager;
		//this.webServerContextRootDirectory = this.scriptingManager.getQuercusScriptEngine().getQuercus().getIniString("web_serv_root");
	}

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
    	//log.debug("Entering doPut");
    	doProcess(req,resp);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
    	//log.debug("Entering doDelete");
    	doProcess(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
    	//log.debug("Entering doPost");
    	doProcess(req,resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
    	//log.debug("Entering doGet");
    	doProcess(req,resp);
    }

    protected void doProcess(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
    	log.debug("Entering doGet");

        try {
	    	processRequestPath(req);
        } catch(Exception e) {
            resp.setContentType("text/plain;charset=UTF-8");
            resp.setStatus(500);
            resp.getWriter().write(e.toString());
    		return;
        }

        if (this.contentType != "php") {
        	resp.setContentType(this.contentType);
            resp.setHeader("Content-Length", "" + this.file.length());
            resp.setHeader("Cache-Control", "max-age=86400");
            resp.setHeader("Content-Disposition", "filename=\""+ this.file.getName() +"\"");
            FileUtils.copyFile(this.file, resp.getOutputStream());
            return;
        }

        if (this.contentType == "php") {
        	resp.setContentType("text/html"); // the default
            resp.setStatus(200);

			try {

				QuercusScriptEngine quercusScriptEngine = scriptingManager.getQuercusScriptEngine();
				QuercusContext quercus = quercusScriptEngine.getQuercus();

			    QuercusPage page;
			    Path pwd = quercus.getPwd().copy();
			    Path path = pwd.lookup(this.file.getCanonicalPath());
			    page = quercus.parse(path);
			    Env env = null;
			    WriteStream ws = null;
			    ws = openWrite(resp);
			    ws.setDisableCloseSource(true);
			    ws.setNewlineString("\n");
			    QuercusHttpServletRequest req1 = new QuercusHttpServletRequestImpl(req);
			    QuercusHttpServletResponse resp1 = new QuercusHttpServletResponseImpl(resp);
			    env = quercus.createEnv(page, ws, req1, resp1);
			    env.setPwd(path.getParent());

			    ApplicationUser loggedInUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
			    if (loggedInUser != null) {
				    quercus.setServerEnv("JIRA_USER_NAME", loggedInUser.getName());
				    quercus.setServerEnv("JIRA_USER_KEY", loggedInUser.getKey());
			    } else {
				    quercus.setServerEnv("JIRA_USER_NAME", "");
				    quercus.setServerEnv("JIRA_USER_KEY", "");
			    }
			    env.setGlobalValue("httpServletRequest", env.wrapJava(req));
			    env.setGlobalValue("httpServletResponse", env.wrapJava(resp));
			    env.setGlobalValue("loggedInUser", env.wrapJava(loggedInUser));
			    env.setGlobalValue("log", env.wrapJava(log));
		        env.start();
		        env.executeTop();
		        if (env != null)
		            env.close();
		          // don't want a flush for an exception
		          if (ws != null && env.getDuplex() == null)
		            ws.close();
	            return;

			} catch (Exception ex) {
	        	resp.setContentType("text/plain"); // the default
	            resp.setStatus(500);
				log.error("Unrecognized exception while executing PHP script: " + ex.getMessage(), ex);
	            resp.getWriter().write(ex.getMessage());
	            return;
			}

        }

        // actually cannot get here
        resp.setContentType("text/plain;charset=UTF-8");
        StringBuilder testResponce = new StringBuilder ();
        testResponce.append("This is SOMETHING\n");
		resp.getWriter().write( testResponce.toString() );
    }

    private void processRequestPath(HttpServletRequest req) throws Exception {
        // ONLY WHEN: <url-pattern>/phpwebserv/*</url-pattern>
    	// getRequestURI /jira/plugins/servlet/phpwebserv/blah/blah
    	// getContextPath /jira
    	// getServletPath /plugins/servlet/phpwebserv
    	// getPathInfo /blah/blah

    	String pathInfo = req.getPathInfo();
    	log.debug("processRequestPath:" + req.getRequestURI() + " pathInfo: " + pathInfo);

		if (!pathInfo.startsWith("/")) {
			throw new Exception("no any context");
		}
		pathInfo = pathInfo.substring(1);
		int i = pathInfo.indexOf("/");
		if (i < 0) {
			throw new Exception("must have some context");
		}
		this.webServerContext = pathInfo.substring(0, i);
		this.webServerPathInfo = pathInfo.substring(i+1);
    	log.debug("processRequestPath: webServerContext:" + this.webServerContext + " webServerPathInfo:" + this.webServerPathInfo);

    	WebServerContextEntity e = webServerContextEntityManager.getByContext(this.webServerContext);
    	if (e == null) {
			throw new Exception("context not found");
    	}

    	if (e.isRequiresAuthenticatedUser() && !ComponentAccessor.getJiraAuthenticationContext().isLoggedInUser()) {
			throw new Exception("logged in user required");
    	}

    	if (e.isRequiresWebSudo() && !webSudoManager.canExecuteRequest(req)) {
			throw new Exception("websudo session required");
    	}

    	this.webServerContextRootDirectory = e.getDocumentRoot();
    	String realPath = this.webServerContextRootDirectory + "/" + this.webServerPathInfo;
    	log.debug("webServerContextRootDirectory:" + this.webServerContextRootDirectory + " realPath:"+realPath);
    	this.file = new File(realPath);
    	if (!this.file.isFile()) {
			throw new Exception("is not a file");
    	}
    	if (!this.file.canRead()) {
			throw new Exception("cannot read");
    	}
    	String extention = FilenameUtils.getExtension(this.file.getCanonicalPath());
    	this.contentType = extentionsMap.get(extention);
    	if (this.contentType == null) {
			throw new Exception("unsupported extention " + extention);
    	}
    }

    @Override
	protected long getLastModified(HttpServletRequest req) {
    	if (this.file == null)
    		return -1;
    	if (this.contentType == "php")
    		return -1;
    	return this.file.lastModified();
    }

    protected WriteStream openWrite(HttpServletResponse response) throws IOException {
		WriteStream ws;
		OutputStream out = response.getOutputStream();
		ws = Vfs.openWrite(out);
		return ws;
    }
}