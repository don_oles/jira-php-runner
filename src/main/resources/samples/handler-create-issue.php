<?php

$log->warn("Hello from mail handler!");
$deleteEmail = false;

if (!$handler->canHandleMessage($message)) {
    $log->debug("Cannot handle message '$subject'.");
    return $deleteEmail;
}

$project = $handler->getProject(); // ComponentAccessor.getProjectManager().getProjectObjByKey(projectKey)  
if (!$project) {
    return false;
}

$reporter = $handler->getReporter($message);
if (!$reporter) {
    return false;
}

if (!$handler->canCreateIssues($reporter, $project)) {
    return false;
}

//$issueType = $handler->getIssueTypeObjectByName($params["issueType"]); 
$issueType = $handler->getIssueTypeObject(); // ComponentAccessor.getConstantsManager().getIssueTypeObject(issueType)
if (!$issueType) {
    return false;
}

$summary = $message->getSubject();
if (!$summary) {
    $monitor->markMessageForDeletion("No subject, ignoring");
    return false;
}
$summary = $handler->truncateSummary($summary);
$priority = $handler->getPriority($message, $project, $issueType); // also checks field visibility
$description = $handler->getDescription($message, $project, $issueType, $reporter); // also checks field visibility

if ($params["ccassignee"]) {
    $assignee = $handler->getFirstValidAssignee($message->getAllRecipients(), $project);
}
if (!$assignee) {
    $assignee = $handler->getDefaultAssignee($project);
}
if ($assignee) {
    $assigneeId = $assignee->getName(); // $assignee->name does not work for some reason
}
//var_dump($assignee);

$issueObject = $handler->getNewIssueObject();
$issueObject->setProjectObject($project);
$issueObject->setSummary($summary);
if ($description) { $issueObject->setDescription($description); }
$log->warn("Setting issueType to ${issueType->id}/${issueType->name}");
$issueObject->setIssueTypeObject($issueType);
$issueObject->setReporter($reporter);
if ($assigneeId) { $issueObject->setAssigneeId($assigneeId);}
if ($priority) { $issueObject->setPriorityId($priority); }

// Ensure issue level security is correct
$handler->setDefaultSecurityLevel($issueObject); // $issue->setSecurityLevelId($levelId);
$handler->setCustomFieldDefaults($issueObject);
$handler->setCustomFieldValue($issueObject,"customfield_10000","value from handler");
$attachments = $handler->prepareAttachmentsForMessage($message, $issueObject);
foreach($attachments as $att) {
    $log->debug("I see attachment ${att->md5} ${att->fileName}");
    if ($att->md5 == "750DBA108138D707044355813A3D2483") {
        $att->skip("Just because it sucks");
    }
    if ($att->contentType == "text/html" && $att->fileName == "") {
        $log->warn("Found an text/html attachment"); // file_get_contents($att->file->canonicalPath)
        $att->skip(null);
        $att->fileName = rand().".html";
    }
}
$issue = $context->createIssue($reporter, $issueObject);
$handler->addCcWatchersAndAttachments($message, $issue, $reporter);

return true;

?>